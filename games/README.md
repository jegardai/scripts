# Save game management

Collection of scripts in order to move all (as much as i can) save of my
video games in one remote place (eg. Nextcloud directory,…). Then a symbolic
link should take place of the directories. This allows me to :
* Have save in the cloud for all games !
* Easily find my save in one place.
* Easily backup everything (one `tar` to backup them all).
* Synchronize save between Linux hosts.
* Restore save if anything happens to my game computer.
* Everything that Nextcloud allows me to do (access from anywhere, share,…).

## How-to use

Run the main script to automatically run the 3 others :

``` sh
~/repos/gardouille.scripts/games/save.game
```

This will run :
- **save.game.steam** : Try to move or symlink saves from 3 Steam's directories,
userdata, common and compatdata.
- **save.game.xdg** : Try to move or symlink save that respect the XDG
specifications (~/.config and ~/.local/share).
- **save.game.home** : Try to move or symlink other saves that the devs have
seen fit to place anywhere else…

## Currently managed games

* Alien: Isolation               − https://www.pcgamingwiki.com/wiki/Alien:_Isolation
* Bastion                        − https://www.pcgamingwiki.com/wiki/Bastion
* Batman: Arkham Asylum          − https://www.pcgamingwiki.com/wiki/Batman:_Arkham_Asylum
* Batman: Arkham City            − https://www.pcgamingwiki.com/wiki/Batman:_Arkham_City
* Batman: Arkham Origins         − https://www.pcgamingwiki.com/wiki/Batman:_Arkham_Origins
* Borderlands: The Pre-Sequel    − https://www.pcgamingwiki.com/wiki/Borderlands:_The_Pre-Sequel
* Borderlands 2                  − https://pcgamingwiki.com/wiki/Borderlands_2
* Broforce                       − https://pcgamingwiki.com/wiki/Broforce
* Bayonetta                      − https://www.pcgamingwiki.com/wiki/Bayonetta
* Butcher                        − https://pcgamingwiki.com/wiki/Butcher
* Capsized                       − https://pcgamingwiki.com/wiki/Capsized
* Celeste                        − https://www.pcgamingwiki.com/wiki/Celeste
* Castle Crashers                − https://pcgamingwiki.com/wiki/Castle_Crashers
* Children of Morta              − https://www.pcgamingwiki.com/wiki/Children_of_Morta
* Crawl                          − https://www.pcgamingwiki.com/wiki/Crawl
* Crypt of the Necrodancer       − https://pcgamingwiki.com/wiki/Crypt_of_the_Necrodancer
* Darkwood                       − https://pcgamingwiki.com/wiki/Darkwood
* Deadbolt                       − https://www.pcgamingwiki.com/wiki/Deadbolt
* Death Squared                  − https://www.pcgamingwiki.com/wiki/Death_Squared
* Deep Rock Galactic             − https://www.pcgamingwiki.com/wiki/Deep_Rock_Galactic
* Dig or Die                     − https://www.pcgamingwiki.com/wiki/Dig_or_Die
* DmC: Devil May Cry             − https://www.pcgamingwiki.com/wiki/DmC:_Devil_May_Cry
* Doom (2016)                    − https://www.pcgamingwiki.com/wiki/Doom_(2016)
* Door Kickers: Action Squad     − https://pcgamingwiki.com/wiki/Door_Kickers:_Action_Squad
* Duck Game                      − https://pcgamingwiki.com/wiki/Duck_Game
* Dust: An Elysian Tail          − https://www.pcgamingwiki.com/wiki/Dust:_An_Elysian_Tail
* Enter the Gungeon              − https://www.pcgamingwiki.com/wiki/Enter_the_Gungeon
* Fez                            − https://www.pcgamingwiki.com/wiki/Fez
* Full Metal Furies              − https://pcgamingwiki.com/wiki/Full_Metal_Furies
* Galak-Z The Void               − https://www.pcgamingwiki.com/wiki/Galak-Z:_The_Dimensional
* Guacamelee! 2                  − https://www.pcgamingwiki.com/wiki/Guacamelee!_2
* Guns, Gore & Cannoli           − https://www.pcgamingwiki.com/wiki/Guns,_Gore_%26_Cannoli
* Hellblade: Senua's Sacrifice   − https://www.pcgamingwiki.com/wiki/Hellblade:_Senua%27s_Sacrifice
* Hero Siege                     − https://www.pcgamingwiki.com/wiki/Hero_Siege
* HotlineMiami                   − https://pcgamingwiki.com/wiki/Hotline_Miami
* Human: Fall Flat               − https://www.pcgamingwiki.com/wiki/Human:_Fall_Flat
* Hyper Light Drifter            − https://www.pcgamingwiki.com/wiki/Hyper_Light_Drifter
* Jump Gunners                   − https://pcgamingwiki.com/wiki/Jump_Gunners
* Left 4 Dead 2                  − https://www.pcgamingwiki.com/wiki/Left_4_Dead_2
* Mercenary Kings                − https://pcgamingwiki.com/wiki/Mercenary_Kings
* Metal Slug X                   − https://pcgamingwiki.com/wiki/Metal_Slug_X
* MO: Astray                     − https://pcgamingwiki.com/wiki/MO:_Astray
* Monaco                         − https://www.pcgamingwiki.com/wiki/Monaco:_What%27s_Yours_Is_Mine
* N++                            − https://www.pcgamingwiki.com/wiki/N%2B%2B
* Never Alone                    − https://pcgamingwiki.com/wiki/Never_Alone
* Nuclear Throne                 − https://pcgamingwiki.com/wiki/Nuclear_Throne
* Overcooked! 2                  − https://pcgamingwiki.com/wiki/Overcooked!_2
* Ori and the Blind Forest: D−E  − https://www.pcgamingwiki.com/wiki/Ori_and_the_Blind_Forest:_Definitive_Edition
* Ori and the Will of the Wisps  − https://www.pcgamingwiki.com/wiki/Ori_and_the_Will_of_the_Wisps
* Pikuniku                       − https://www.pcgamingwiki.com/wiki/Pikuniku
* PixelJunk Shooter              − https://pcgamingwiki.com/wiki/PixelJunk_Shooter
* Portal 2                       − https://pcgamingwiki.com/wiki/Portal_2 (solo only, multiplayer is on Steam cloud)
* Prey (2017)                    − https://pcgamingwiki.com/wiki/Prey_(2017) (don't work yet)
* Risk of Rain                   − https://pcgamingwiki.com/wiki/Risk_of_Rain
* Saints Row IV                  − https://pcgamingwiki.com/wiki/Saints_Row_IV
* Saints Row: The Third          − https://pcgamingwiki.com/wiki/Saints_Row:_The_Third
* Shadow of the Tomb Raider      − https://www.pcgamingwiki.com/wiki/Shadow_of_the_Tomb_Raider
* Shift Happens                  − https://pcgamingwiki.com/wiki/Shift_Happens (don't work yet)
* South Park: The Stick of Truth − https://pcgamingwiki.com/wiki/South_Park:_The_Stick_of_Truth
* SpeedRunners                   − https://www.pcgamingwiki.com/wiki/SpeedRunners
* Steam Screenshots              − https://steamdb.info/app/760/
* Strange Brigade                − https://www.pcgamingwiki.com/wiki/Strange_Brigade
* Streets of Rogue               − https://www.pcgamingwiki.com/wiki/Streets_of_Rogue
* The Dishwasher: Vampire Smile  − https://pcgamingwiki.com/wiki/The_Dishwasher:_Vampire_Smile
* The Forest                     − https://www.pcgamingwiki.com/wiki/The_Forest
* The Swapper                    − https://pcgamingwiki.com/wiki/The_Swapper
* TowerFall Ascension            − https://pcgamingwiki.com/wiki/TowerFall_Ascension (not managed cause no substree, check $XDG_DATA_HOME)
* Tricky Towers                  − https://pcgamingwiki.com/wiki/Tricky_Towers
* Trine 2 Complete Story         − https://pcgamingwiki.com/wiki/Trine_2
* Trine Enchanted Edition        − https://pcgamingwiki.com/wiki/Trine_Enchanted_Edition
* Trine 4                        − https://pcgamingwiki.com/wiki/Trine_4:_The_Nightmare_Prince
* Ultimate Chicken Horse         − https://www.pcgamingwiki.com/wiki/Ultimate_Chicken_Horse
* Valheim                        − https://www.pcgamingwiki.com/wiki/Valheim
* Victor Vran                    − https://www.pcgamingwiki.com/wiki/Victor_Vran
* Wizard of Legend               − https://www.pcgamingwiki.com/wiki/Wizard_of_Legend

# Not managed games

### Everything is in the cloud

* Killing Floor 2                − https://www.pcgamingwiki.com/wiki/Killing_Floor_2
* Move or Die (cloud)            − https://www.pcgamingwiki.com/wiki/Move_or_Die

### Can't find any data

* Learn Japanese To Survive! Katakana War − https://www.pcgamingwiki.com/wiki/Learn_Japanese_To_Survive!_Katakana_War

## How-to add new save

Sources of informations :
* Various informations (save path,…) : https://pcgamingwiki.com/wiki
* Match steam id and game name : https://steamdb.info/
