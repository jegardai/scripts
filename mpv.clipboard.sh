#!/bin/sh
#
# Purpose {{{
# This script will …
#   1. Run MPV with clipboard content
# …
#
# 2023-01-12
# }}}
# Vars {{{
PROGNAME=$(basename "${0}"); readonly PROGNAME
PROGDIR=$(readlink -m $(dirname "${0}")); readonly PROGDIR
ARGS="${*}"; readonly ARGS
readonly NBARGS="${#}"
[ -z "${DEBUG}" ] && DEBUG=1
## Export DEBUG for sub-script
export DEBUG

## Default values for some vars
LINK_URL_DEFAULT=$(xclip -out -selection clipboard)
readonly LINK_URL_DEFAULT

## Colors
readonly PURPLE='\033[1;35m'
readonly RED='\033[0;31m'
readonly RESET='\033[0m'
readonly COLOR_DEBUG="${PURPLE}"
# }}}
usage() {                                                       # {{{

	cat <<- HELP
usage: $PROGNAME [-d|-h]

Start MPV with clipboard content

EXAMPLES :
    - Play the best YT video
        ${PROGNAME} https://invidious.snopyta.org/watch?v=o-YBDTqX_ZU

OPTIONS :
    -d,--debug
        Enable debug messages.

    -h,--help
        Print this help message.

    -l,--link
        Play specific link (default : Use clipboard)
        So basically useless and better use MPV directly.
HELP
}
# }}}
debug_message() {                                               # {{{

	local_debug_message="${1}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '\e[1;35m%-6b\e[m\n' "DEBUG − ${PROGNAME} : ${local_debug_message}"

	unset local_debug_message

	return 0
}
# }}}
error_message() {                                               # {{{

	local_error_message="${1}"
	local_error_code="${2}"

	## Print message
	printf '%b\n' "ERROR − ${PROGNAME} : ${RED}${local_error_message}${RESET}"

	unset local_error_message

	exit "${local_error_code:=66}"
}
# }}}
define_vars() {                                                 # {{{

	# If link_url wasn't defined (argument) {{{
	if [ -z "${link_url}" ]; then
	 ### Use default value
	 readonly link_url="${LINK_URL_DEFAULT}"
	fi
	# }}}

	true; ## Remove me
}
# }}}

main() {                                                        # {{{

	debug_message "--- MAIN BEGIN"

	## If script should not be executed right now {{{
	### Exit
	#is_script_ok \
		#&& exit 0
	## }}}

	## Define all vars
	define_vars
	debug_message "| Define vars"

	sh -c "mpv --input-ipc-server=${HOME}/.cache/mpv.clipboard.socket ${link_url}"

	debug_message "--- MAIN END"
}
# }}}

# Manage arguments                                                # {{{
# This code can't be in a function due to argument management

if [ ! "${NBARGS}" -eq "0" ]; then

	manage_arg="0"

	## If the first argument is not an option
	if ! printf -- '%s' "${1}" | grep -q -E -- "^-+";
	then
		## Print help message and exit
		printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
		printf '%b\n' "---"
		usage

		exit 1
	fi

	# Parse all options (start with a "-") one by one
	while printf -- '%s' "${1}" | grep -q -E -- "^-+"; do

	case "${1}" in
		-d|--debug )                          ## debug
			DEBUG=0
			debug_message "--- Manage argument BEGIN"
			;;
		-h|--help )                           ## help
			usage
			## Exit after help informations
			exit 0
			;;
		-l|--link )                           ## Define link_url with given arg
			## Move to the next argument
			shift
			## Define var
			readonly link_url="${1}"
			;;
		* )                                   ## unknow option
			printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
			printf '%b\n' "---"
			usage
			exit 1
			;;
	esac

	debug_message "| ${RED}${1}${COLOR_DEBUG} option managed."

	## Move to the next argument
	shift
	manage_arg=$((manage_arg+1))

	done

	debug_message "| ${RED}${manage_arg}${COLOR_DEBUG} argument(s) successfully managed."
else
	debug_message "| No arguments/options to manage."
fi

	debug_message "--- Manage argument END"
# }}}

main

exit 0
