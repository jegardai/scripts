#!/bin/bash
######################################################
## Petit script qui se charge de synchroniser mon wiki
##  distant dans un répertoire local pour utilisation
##  offline.
######################################################

# Le serveur où est stocker le wiki
serveur="straga"
user="darker"

# Test si présent sur le réseau
ping -c 2 "${serveur}" > /dev/null

if [ $? -eq 0 ]; then
  rsync -az "${user}"@"${serveur}":/var/www/dokuwiki /home/"${user}"/repos/ > /dev/null 2> /dev/null
  if [ $? -eq 0 ]; then
    echo "Synchro du wiki ${serveur} réussie"
  else
    echo "Synchro échouée"
  fi
else
  echo "Not @home"
fi

