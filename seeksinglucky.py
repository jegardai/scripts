#!/usr/bin/env python
import urllib
import json
import sys
import os
proxy = {'http': 'http://localhost:8250'}

node="http://s.s/search?output=json&action=expand&expansion=1&q="


if __name__=="__main__":
    query_str = sys.argv[1:]
    query = urllib.quote("+".join(query_str))
    raw_page = urllib.urlopen(node + query, proxies=proxy)
    page = raw_page.read()
    try:
        content = json.loads(page)
    except:
        raise
        print("Server's JSON is corrupted")
    snippets = content["snippets"]
    if len(snippets) == 0:
        print('No results')

    os.system('iceweasel %s' % snippets[0]['cite'])
    #os.system('x-www-browser %s' % snippets[0]['cite'])
