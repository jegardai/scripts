#!/bin/bash
################################################################################################
## 
##  Nom: dl_plowshare.sh 
##
##  Version: 0.1
##
##  Licence: Creative Commons
##
##  Comportement: Analyse un répertoire pour savoir si il contient un fichier .down qui
##    contiendra des liens ddl (megaupload, DL_FILE). Créée un dossier du même nom que le fichier et
##    y télécharge les liens qu'il contient avec plowshare. Change ensuite l'extension du fichier
##    en .old pour qu'il ne soit pas retéléchargé.
##
##  /!\ Changer la variable DL_DIR /!\
##
##  Appliquer une tache cron du style:
##  */2 * * * * /chemin/script/dl_plowshare.sh
##
##
##
################################################################################################
##
##  Date de création: 03-10-2011
##
##  Auteur: Gardouille
##
##
##  Dernière modifications:
##  03-10-2011 - 0.1:
##    - 
# **********************************************************************************************
##  //2011 - :
##    - 
##    - 
# **********************************************************************************************
##
##  À modifier:
##    - Possibilité de récupérer le répertoire à analyser en argument
##
##
################################################################################################



# **********************************************************************************************
# 
# Variables globales
# 
# -----------------------------------------------------------
# Fichier temporaire pour savoir si le script est déjà en cours
TMP_FILE="/tmp/dl_plowshare"
# Répertoire à analyser pour trouver un fichier contenant une liste de lien
DL_DIR="/media/data/download/ddl"
# Nom du fichier (sans l'extension) contenant les liens à télécharger
#BASE_DL_FILE=""
# Extension de fichier à analyser  
EXTENSION="down"

# Fin des variables globales
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
# 
# Fichiers globaux
# 
# -----------------------------------------------------------
# 

# Fin des fichiers globaux
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
# 
# Fonctions globales
# 
# -----------------------------------------------------------
# Divers echos DL_FILE
echoi() { echo " (ii) $*" >> "${log}" ; }
echok() { echo " (ok) $*" >> "${log}" ; }
echow() { echo " (!!) $*" >> "${log}" ; }
echnk() { echo " (EE) $*" >> "${log}" ; }

# Fin des fonctions globales
# -----------------------------------------------------------
# **********************************************************************************************



# **********************************************************************************************
# 
# Programme principale
# 
# -----------------------------------------------------------

## Vérifier que le script n'est pas déjà en cours
if [ ! -f "${TMP_FILE}" ]; then


  ## Créer un fichier temporaire pour indiquer que le script est en cours d'exécution
  touch "${TMP_FILE}"

  ## Se placer dans le dossier à analyser
  pushd "${DL_DIR}" > /dev/null

  ## Analyser le dossier pour récupérer UN fichier .down (nom de base + extension)
  DL_FILE=`\ls | \grep ."${EXTENSION}"|head -n 1`

  ## Récupérer le nom du fichier (sans extension)
  BASE_DL_FILE=${DL_FILE%.*}

  ## Créer un dossier du même nom et se placer dedans
  mkdir "${BASE_DL_FILE}"
  pushd "${DL_DIR}"/"${BASE_DL_FILE}" > /dev/null

  ## Déplacer le fichier .down dans le dossier
  mv "${DL_DIR}"/"${DL_FILE}" .

  ## Lancer le téléchargement en passant en argument le fichier .down
  plowdown -m "${DL_FILE}"

  ## Ressortir du dossier de téléchargement
  popd > /dev/null

  ## Ressortir de l'emplacement à analyser
  popd > /dev/null

  ## Supprimer le fichier temporaire
  rm "${TMP_FILE}"

else
  ## Script déjà en cours
  echo "Script de téléchargement déjà en cours."
fi

exit 0
# Fin de la boucle principale
# -----------------------------------------------------------
# **********************************************************************************************


