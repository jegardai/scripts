#!/bin/sh
#
# Purpose {{{
# This script will try to send multimedia controls to correct player
#   1. Send selected command to MPV if running
#   2. Send selected command to Kodi if running
#   3. Send selected command to MPD if running
#   4. Start mpd if nothing runs
# …
#
# 2022-05-22
# }}}
# Vars {{{
readonly PROGNAME=$(basename "${0}")
readonly PROGDIR=$(readlink -m $(dirname "${0}"))
readonly ARGS="${*}"
readonly NBARGS="${#}"
[ -z "${DEBUG}" ] && DEBUG=1
## Export DEBUG for sub-script
export DEBUG

## Ensure current directory is in the path
#  to be able to call sub-scripts
PATH="${PATH}:${PROGDIR}"

## Default values for some vars
readonly MPV_PROC_REGEXP="^mpv.*--input-ipc-server="
readonly KODI_PROC_REGEXP="kodi.bin"
readonly MPD_PROC_REGEXP="mpd.*systemd"

readonly MULTIMEDIA_COMMAND_DEFAULT="toggle"

## Colors
readonly PURPLE='\033[1;35m'
readonly RED='\033[0;31m'
readonly RESET='\033[0m'
readonly COLOR_DEBUG="${PURPLE}"
# }}}
usage() {                                                       # {{{

	cat <<- HELP
usage: $PROGNAME [-c|-d|-h]

This script will try to send multimedia controls to correct player
   1. Send selected command to MPV if running
     * Latest instance only with an IPC socket.
   2. Send selected command to Kodi if running
   3. Send selected command to MPD if running
   4. Start mpd if nothing runs

EXAMPLES :
    - Send TOGGLE command to the first media player running
        ${PROGNAME} --command toggle

OPTIONS :
    -c,--command
        Send some command to running media player (MPV, Kodi, MPD,…).

    -d,--debug
        Enable debug messages.

    -h,--help
        Print this help message.

HELP

}
# }}}
debug_message() {                                               # {{{

	local_debug_message="${1}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '\e[1;35m%-6b\e[m\n' "DEBUG − ${PROGNAME} : ${local_debug_message}"

	unset local_debug_message

	return 0
}
# }}}
error_message() {                                               # {{{

	local_error_message="${1}"
	local_error_code="${2}"

	## Print message
	printf '%b\n' "ERROR − ${PROGNAME} : ${RED}${local_error_message}${RESET}"

	exit "${local_error_code:=66}"
}
# }}}
define_vars() {                                                 # {{{

	## If MULTIMEDIA_COMMAND wasn't defined (argument) {{{
	if [ -z "${MULTIMEDIA_COMMAND}" ]; then
		## Use default value
		readonly MULTIMEDIA_COMMAND="${MULTIMEDIA_COMMAND_DEFAULT}"
	fi
	## }}}

}
# }}}
is_proc_absent() {                                             # {{{

	local_proc_pattern="${1}"

	## Proc is absent by default
	return_proc_absent="0"

	local_count_proc_pattern="$(pgrep -f -- "${local_proc_pattern}" | wc -l)"

	case "${local_count_proc_pattern}" in
		0 ) ## No procs related to this pattern are running
			return_proc_absent="0"
			;;
		* ) ## At least one proc seems running
			return_proc_absent="1"
			;;
esac

	## Simple debug message to valid current variables
	debug_message "is_proc_absent − \
procs running (with the pattern: ${RED}${local_proc_pattern}${COLOR_DEBUG}) on the current host: ${RED}${local_count_proc_pattern}${COLOR_DEBUG}."

	return "${return_proc_absent}"

}
# }}}
is_proc_running() {                                             # {{{

	local_proc_pattern="${1}"

	local_count_proc_pattern="$(pgrep -f -- "${local_proc_pattern}" | wc -l)"

	case "${local_count_proc_pattern}" in
		0 ) ## No procs related to this pattern are running
			return_proc_running="1"
			;;
		* ) ## At least one proc seems running
			return_proc_running="0"
			;;
esac

	## Simple debug message to valid current variables
	debug_message "is_proc_running − \
procs running (with the pattern: ${RED}${local_proc_pattern}${COLOR_DEBUG}) on the current host: ${RED}${local_count_proc_pattern}${COLOR_DEBUG}."

	return "${return_proc_running}"

}
# }}}

main() {                                                        # {{{

	## Define all vars
	define_vars

	## If MPV with an IPC-socket is running {{{
	### AND Send multimedia command to MPV sub-script
	### AND exit 0
	is_proc_running "${MPV_PROC_REGEXP}" \
		&& debug_message "Try to send ${RED}${MULTIMEDIA_COMMAND}${COLOR_DEBUG} to ${RED}MPV${COLOR_DEBUG}." \
		&& mpv.controller.sh --command "${MULTIMEDIA_COMMAND}" \
		&& exit 0
	## }}}
	## If kodi is running {{{
	### AND Send multimedia command to Kodi sub-script
	### AND exit 0
	is_proc_running "${KODI_PROC_REGEXP}" \
		&& debug_message "Try to send ${RED}${MULTIMEDIA_COMMAND}${COLOR_DEBUG} to ${RED}Kodi${COLOR_DEBUG}." \
		&& kodi.controller.sh --command "${MULTIMEDIA_COMMAND}" \
		&& exit 0
	## }}}
	## If mpd is running {{{
	### AND Send multimedia command to MPD sub-script
	### AND exit 0
	is_proc_running "${MPD_PROC_REGEXP}" \
		&& debug_message "Try to send ${RED}${MULTIMEDIA_COMMAND}${COLOR_DEBUG} to ${RED}MPD${COLOR_DEBUG}." \
		&& mpd.controller.sh --command "${MULTIMEDIA_COMMAND}" \
		&& exit 0
	## }}}
	## If mpd is not running {{{
	### AND Start it with user's systemd
	### AND exit 0
	is_proc_absent "${MPD_PROC_REGEXP}" \
		&& debug_message "Start default player ${RED}mpd with systemd${COLOR_DEBUG}." \
		&& systemctl --user start mpd.service \
		&& exit 0
	## }}}

}
# }}}

# Manage arguments                                                # {{{
# This code can't be in a function due to argument management

if [ ! "${NBARGS}" -eq "0" ]; then

	manage_arg="0"

	## If the first argument is not an option
	if ! printf -- '%s' "${1}" | grep -q -E -- "^-+";
	then
		## Use it as default multimedia command
		MULTIMEDIA_COMMAND="${1}"
	fi

	# Parse all options (start with a "-") one by one
	while printf -- '%s' "${1}" | grep -q -E -- "^-+"; do

	case "${1}" in
		-c|--command )                        ## Define MULTIMEDIA_COMMAND
			## Move to the next argument
			shift
			## Define var
			readonly MULTIMEDIA_COMMAND="${1}"
			;;
		-d|--debug )                          ## debug
			DEBUG=0
			;;
		-h|--help )                           ## help
			usage
			## Exit after help informations
			exit 0
			;;
		* )                                   ## unknow option
			printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
			printf '%b\n' "---"
			usage
			exit 1
			;;
	esac

	debug_message "Arguments management − \
${RED}${1}${COLOR_DEBUG} option|command managed."

	## Move to the next argument
	shift
	manage_arg=$((manage_arg+1))

	done

	debug_message "Arguments management − \
${RED}${manage_arg}${COLOR_DEBUG} argument(s) successfully managed."
else
	debug_message "Arguments management − \
No arguments/options to manage."
fi

# }}}

main

exit 255
