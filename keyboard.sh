#!/bin/sh

# Manage keyboard disposition for TypeMatrix if keyboard is available.
TM_ID="1e54:2030"
TM_AVAILABLE=$(lsusb | grep -i -- "$TM_ID")

if [ "${TM_AVAILABLE}" ]; then
	setxkbmap -model tm2030USB -layout fr -variant bepo \
	&& exit 0
fi

# Manage keyboard disposition for Dell if it's a Dell
IS_DELL=$(lsusb | grep -i -- "dell")
IS_LAPTOP=$(grep -qE "^10" /sys/devices/virtual/dmi/id/chassis_type)
if [ "${IS_DELL}" ]; then
	# Manage keyboard disposition for Dell laptop if it's a laptop
	if [ "${IS_LAPTOP}" ]; then
		setxkbmap -model latitude -layout fr -variant bepo \
		&& exit 0
	else
		# Default Dell map to pc104 and bepo
		setxkbmap -model pc104 -layout fr -variant bepo \
		&& exit 0
	fi
else
	# Default map to pc104 and bepo
	setxkbmap -model pc104 -layout fr -variant bepo \
	&& exit 0
fi


exit 255
