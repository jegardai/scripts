# Scripts

## Description:
Some useful scripts (for me) that can be added to $PATH :)

## List:
* apt-fast: Script for faster package downloading with 'axel'.
* bash_quote: Get a random quote from http://danstonchat.com
* droopy: Small webserver to allow upload on your machine.
* firewall: A script shell to set some iptables rules.
* update-dynmotd.d/: scripts to update the motd (via the /etc/update-motd.d directory).
* flac_to_mp3: convert all flac files of a directory into mp3.
* launch_thyme : Launch Thyme - apps tracker.
* num_circle: Transform a number into a digit with a circle.
* pomodorrior : A tiny pomodoro's timer for Taskwarrior.
* pomodoro: Print a task and a timer in a file. Try to apply Pomodoro Technique!
* snapsend.sh: Send a ZFS snapshot to a remote host.
* tag_photo.sh : Add an exif tag to images.
* test_ssl3: Test if a website supportes the SSLV3 protocol.
* thyme : Automatically track which applications you use and for how long.
* turtl : Symlink to my Turtl app (evernote alternative).
* veille.sh: Kill every sensitive process and files then lock the screen.
* vimmanpager: Tiny script can be used as PAGER, it will call VIM!
* weblink: Tiny Python script that run a small webserver to share files.
* winboot : Reboot the system to a windaube partition.
* wol: Send WakeOnLan/magic packets to remote host.
* zenity_generator: Script to generate zenity window.
* zfSnap.sh: Take snapshot of a ZFS pool.

### Apt-fast
Speed-up APT packages downloading with 'axel' (light command line download accelerator). Juste use it like aptitude/apt/apt-get.
```sh
sudo apt-fast full-upgrade
```

### Bash_quote
Get a random quote from http://danstonchat.com with 'lynx'.

### Droopy
Run a small webserver and allow user to upload files to a local directory:wq


### Firewall
A shell script to apply some Iptables rules.
  * Rules are automatically apply only for the UP interfaces.
  * If it's detect some softwares (eg. Apache2), Iptables rules for ports 80/443 are automatically apply.
  * Special rules allow all traffic throught a VPN:
```sh
firewall vpn
```
  * If a local file exists (firewall.local) it will also apply it. For personnal rules for example.

### Flac_to_mp3
Convert FLAC audio files to MP3 with 'avconv'.

  * Convert a directory:
```sh
flac_to_mp3 /media/data/bisounours_land_v2
```
  * Convert a file:
```sh
flac_to_mp3 /media/data/makarena.flac
```

### Launch_thyme
A tiny script to launch Thyme (apps tracker) to be sure to avoid recording when the screen is lock,…


### Num_circle
Tiny Bash script that take a number between 0 and 20 as argument and transform it into a digit with into a circle.
```sh
num_circle 18
⑱
```

### Pomodorrior
A tiny timer to use the Pomodoro technique with Taskwarrior.

The script will :
  * Create the task if it doesn't exists.
  * Start the task for 25 minutes (default time).
  * Start a break time for 5 minutes (default time).

All informations will be printed in the Taskwarrior's directory : **${HOME}/.task/.current.task** so you can then parse and print the content of this file into your systray, tmux's statusbar,…

### Pomodoro
My implementation of the Pomodoro Technique (https://en.wikipedia.org/wiki/Pomodoro_Technique).

You can simply launch it with:
```sh
pomodoro "Work my french kiss"
```

Then the script will:
* Create a ~/.pomodoro directory to store current task (current.task) and a summary of each week (eg week-42-2015.txt).
* First, it's put task name and a timer (for $WORK_TIME) to the current task file.
* Once the $WORK_TIME has been reached:
  1. Log task name and worked time to the weekly log file
  2. Toggle the sound to mark a pause
  3. It's put a pause/break message for 5/20 minutes as current task
  4. Delete the current task file

#### Disavantages
* Must run the script every ~30 minutes
* …?

#### Advantages
* I can display my current task and it's timer wherever i want (tmux, herbstluftwm, …)
* Written to work with /bin/sh

## rename_meurice_podcast
I download some podcast from a RSS flux (http://www.franceinter.fr/emission-le-moment-meurice) but i don't like :
* the filename
* the title tag

So it's a tiny script to correct this. I get the date and the "real" title from the "title" tag and :
* The filename become "$date_$realtitle.mp3"
* The title tag become "$date_$realtitle"

"Le moment Meurice" : http://www.franceinter.fr/emission-le-moment-meurice

The script manage both a directory and a file as first argument.

## tag_photo.sh
The main goal is to have a solution to know the "subject" of a picture without to open it and no really good name. It can happen if you delete your pictures and get it back with `photorec` or `foremost`. For this i use the Exif metadata.

You can simply launch the script with your pictures directory :

``` sh
tag_photo.sh /tmp/Images
```

Then the script will :
* List all subdirectories.
* Get the basename of each directory and add it as the value of the Exif "description" tag for each images.

To see the result, you can display the tags "File Name" and "Description" of you images :

```sh
find /tmp/Images -type f -iregex '.*\.\(jpg\|gif\|png\|jpeg\)$' -exec exiftool {} -FileName -Description \;
```

## Test_ssl3
Redhat's script to test if an LDAP server support SSLv3.

You could also use a nmap command:
```sh
nmap --script ssl-enum-ciphers -p 443 ldap.tld.org | grep "SSLv3: No supported ciphers found"
```

### Thyme
A tracker to know which applications you use and for how long. It's just the binary from the [Github's repository][thyme repo].

I launch it with `launch_thyme` to avoid to record when the screen is lock. Everything is called with a Systemd unit (service and timer).

[thyme repo]: https://github.com/sourcegraph/thyme

### Turtl
Simple symlink to my turtl app.

See [Turtl's official website][turtl website] for more informations.

[turtl website]: https://turtlapp.com/

## Weblink
Python program that run a small webserver (BaseHTTPServer) to easily share some files.
  * eg:
```sh
weblink /tmp/goodbye.file
HTTP server running at http://localhost:8888/
^C^C received, shutting down server
```
  * With a password:
```sh
weblink --pass=die /tmp/kitty.file
HTTP server running at http://localhost:8888/die
```

## Winboot
Tiny script to reboot the system to a windaube partition.

Be sure to set the correct value to **WIN_GRUB** variable which must be the number of the windoze partition in Grub. Be careful, the grub's entries start at 0.

* The script will also modify the grub configuration :
  * Set Grub in "saved" mode.
  * Ensure to set the previous default entry with `grub-set-default`.
* Then for the reboot :
  * Choose the grub entry for the next reboot.
	* Reboot the system with ``systemctl``.


