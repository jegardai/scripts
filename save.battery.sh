#!/bin/sh

# Stop some services
sudo systemctl stop cups.service
sudo systemctl stop lxcfs.service
sudo systemctl stop netdata.service
sudo systemctl stop smartd.service
sudo systemctl stop ssh.service

# Set a correct brightness
sudo sh -c "echo 400 > /sys/devices/pci0000:00/0000:00:02.0/drm/card0/card0-LVDS-1/intel_backlight/brightness"
