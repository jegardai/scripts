#!/bin/sh
# .. vim: foldmarker=[[[,]]]:foldmethod=marker

# This script will try to:
## Get first argument
## Or get clipboard content if no argument
## Send this data to a file hosting service
## Print the generate url that hosting the content to stdout
## Or put it in the clipboard if no argument was given

# Vars [[[
debug=false
flag_clipboard=false
flag_inline=false
flag_file=false
flag_url=false
flag_text=false

null_service_url="https://null.101010.fr"

## Colors [[[
c_redb='\033[1;31m'
c_magentab='\033[1;35m'
c_reset='\033[0m'
## ]]]

# ]]]

# Functions
# Function to print a debug message [[[
debug_message() {
	_message="${1}"

	[ "${debug}" = "true" ] && printf "${c_magentab}%-6b${c_reset}\n" "DEBUG ${_message}"
}
# ]]]
# Function to check if the argument is a existent file [[[
is_file_exist() {
	_file="${1}"

	## If it's a non-empty file
	if [ -s "${_file}" ]; then
		flag_file=true
		debug_message "− Func is_file_exist : ${_file} seems to be a file and it's non-empty."
	else
		debug_message "− Func is_file_exist : The content is not a file."
	fi
}
# ]]]
# Function to check if the argument looks like a remote URL [[[
is_remote_url() {
	_url="${1}"

	## The variable will be considered as an URL if it's :
	##   Start with 1 to 5 alpha caracters followed by "://"
	##   Start with "www"
	if printf -- '%s' "${_url}" | grep -q -E -- "^([[:alpha:]]{1,5}://|www)"
	then
		### Verify doesn't already comes from the 0x0 service
		if printf -- '%s' "${_url}" | grep -q -E -- "${null_service_url}"
		then
			flag_url=false
			debug_message "− Func is_remote_url : The url (${_url}) seems to already point to the 0x0 service (${null_service_url})."
		else
			flag_url=true
			debug_message "− Func is_remote_url : The url (${_url}) seems to be a remote url."
		fi
	else
		debug_message "− Func is_remote_url : The content is not an URL."
	fi

}
# ]]]
# Function to get content type [[[
determine_content_type() {
	_content="${1}"

	_content_nb_line=$(printf -- '%s\n' "${_content}" | wc -l)
	debug_message "− Determine type : Content has ${_content_nb_line} line(s) to send."

	## If it's a one line content
	if [ "${_content_nb_line}" -eq "1" ]; then
		### Verify if it's a path to a existent file
		is_file_exist "${_content}"
		### Verify if it's a remote URL
		is_remote_url "${_content}"
	fi
}
# ]]]

# Verify argument [[[
case "$#" in
	0 )
		flag_clipboard=true
		debug_message "− Verify arg : No argument was given, try to use clipboard."
		;;
	1 )
		flag_inline=true
		debug_message "− Verify arg : One argument was given, use it."
		;;
	* )
		printf "${c_redb}%b${c_reset}\n" "Error : Expect one argument or a content in clipboard."
		exit 1
		;;
esac
# ]]]

# Get the content to be sended [[[
# Try to get content from first argument
if [ "${flag_inline}" = "true" ]; then
	content_to_send="${1}"
fi
# Try to get content from clipboard
if [ "${flag_clipboard}" = "true" ]; then
	content_to_send=$(xclip -out -selection clipboard)
fi

debug_message "− Get Content : content to be sended : ${content_to_send}"
# ]]]

# Try to determine the type of the content (file, remote URL or text)
determine_content_type "${content_to_send}"

# Try to send the content according to it's type [[[
if [ "${flag_file}" = "true" ]; then
	## Command to send a local file to 0x0 service
	debug_message "− send data : Send it as a file"
	content_url=$(curl -sF"file=@${content_to_send}" "${null_service_url}")
elif [ "${flag_url}" = "true" ]; then
	## Command to send a remote URL to 0x0 service
	debug_message "− send data : Send it as a remote URL"
	content_url=$(curl -sF"url=${content_to_send}" "${null_service_url}")
else
	## Otherwise, it's a content
	flag_text=true
	debug_message "− send data : Send it as a text"
	content_url=$(printf "%b" "${content_to_send}" | curl -sF'file=@-;' "${null_service_url}")
fi
# ]]]

# Manage the result [[[
## If the URL should simply be printed to stdout
if [ "${flag_inline}" = "true" ]; then
	debug_message "− Manage result : Print the result on stdout :"
	printf "%b\n" "${content_url}"
fi
# If the URL should remplace the previous content of the clipboard
if [ "${flag_clipboard}" = "true" ]; then
	debug_message "− Manage result : Put the result in clipboard."
	echo "${content_url}" | xclip -rmlastnl -selection clipboard
fi
# ]]]

exit 0
