#!/bin/sh
# .. vim: foldmarker=[[[,]]]:foldmethod=marker

# This script will try to:
## Get arguments (at least one)
## Search contact that corresponding to arguments with khard
## Display _only_ the email address of the first match

# Vars [[[
debug=false

## Colors [[[
c_redb='\033[1;31m'
c_magentab='\033[1;35m'
c_reset='\033[0m'
## ]]]

# ]]]

# Functions
# Function to print a debug message [[[
debug_message() {
	_message="${1}"

	[ "${debug}" = "true" ] && printf "${c_magentab}%-6b${c_reset}\n" "DEBUG ${_message}"
}
# ]]]

# Verify argument [[[
debug_message "− Verify arguments : Script get ${#} argument(s)."
case "$#" in
	0 )
		printf "${c_redb}%b${c_reset}\n" "Error : Expect at least one argument."
		exit 1
		;;
	1 )
		contact_info="${1}"
		debug_message "− Look for a contact with these informations:\n${contact_info}"
		{
			contact_line=$(khard email "${contact_info}" | sed -n 2p)
		} > /dev/null
		;;
	2 )
		contact_info="${1}"
		filter_pattern="${2}"
		debug_message "− Look for a contact with these informations:\n${contact_info}\nand filter results with:\n${filter_pattern}"
		{
			contact_line=$(khard email "${contact_info}" | grep -- "${filter_pattern}" | sed -n p)
		} > /dev/null
	;;
	* )
		printf "${c_redb}%b${c_reset}\n" "Error : Expect between 1 and 2 arguments."
		exit 2
		;;
esac
# ]]]

if [ -n "${contact_line}" ]; then
	debug_message "Matching contact:\n${contact_line}"

	contact_mail=$(printf '%s' "${contact_line}" | awk '{print $NF}')
	debug_message "Mail of this contact:\n${contact_mail}"
else
	debug_message "− Found no email addresses corresponding to the argument(s)."
	contact_mail="NO_CONTACT"
fi

	# Print the message to X11
	printf '%s' "${contact_mail}" | xdotool type --clearmodifiers --file -

exit 0
