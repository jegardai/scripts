#!/bin/sh
################################################################################################
##
##  Nom: backup_pirate_usb.sh
##
##  Version: 0.2
##
##  Licence: Creative Commons
##
##  Comportement: Tentative de sauvegarde de données vers la clé usb en utilisant les
##    règles udev
##
##
##
################################################################################################
##
##  Date de création: 03-01-2012
##
##  Auteur: Gardouille
##
##
##  Dernière modifications:
##  03-01-2012 - 0.1:
##    - Création du script
##  04-01-2012 - 0.2:
##    - Ajout d'une boucle for qui parcourt les fichiers et les créée si nécessaire
##    - Utilisation de exec pour rediriger les sorties stdout et stderr
##      - cf http://abs.traduc.org/abs-5.0-fr/ch19.html#redir1
##    - Création de fonction pour rediriger et restaurer les sorties
##    -
# **********************************************************************************************
##  //2011 - :
##    -
##    -
# **********************************************************************************************
##  À Modifier:
##    - Redéfinir les paramètres choisi en fonction du mode debug pour utiliser les
##        fonctions de redirection des sorties
##    -
##
##
################################################################################################



# **********************************************************************************************
#
# Variables globales
#
# -----------------------------------------------------------
# 

## Choix du mode d'exécution du script:
## Niveau 0 (mode_debug=0)
# Exécution automatique avec normalement aucune interaction avec l'utilisateur.
## Niveau 1 (mode_debug=1)
# Exécution semi-automatique avec peu d'interaction nécessaire
## Niveau 2 (mode_debug=2)
# La sortie des différentes commandes est envoyée sur la sortie standard
mode_debug=2

case "$mode_debug" in
  0) ## Niveau minimum
    # La sortie standard sera redirigée dans un fichier de log
    redirect_stdout
    # La sortie d'erreur sera redirigée dans un fichier d'erreur
    redirect_stderr
    ;;
  1) ## Niveau moyen
    # Seulement la sortie d'erreur sera redirigée
    redirect_stderr
    ;;
  2) ## Niveau maximum
    # Le résultat des commandes reste sur la sortie standard
    echo "Mode debug"
    ;;
esac


# Partition à monter
PART="/dev/tipiak"

# Point de montage
MOUNT_POINT="/mnt/bkp_tipiak"

## Répertoire à sauvegarder
DIR="/media/data/config_debian/sid"

## Répertoire où s'effectue la sauvegarde (sur la clé usb)
BKP_DIR="backup"

## Obtenir le volume id d'un périphérique:
#hal-find-by-property --key block.device --string /dev/sdXX

## Obtenir le point de montage d'un périphérique à partir de son volume id:
#MOUNTPOINT=$(hal-get-property --udi /org/freedesktop/Hal/devices/le_volume_uuid_detrminé_precedemment --key volume.mount_point)

# Fin des variables globales
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fichiers globaux
#
# -----------------------------------------------------------
## Fichier de log
fichier_log="/tmp/backup_pirate_usb.log"
## Fichier de erreur
fichier_erreur="/tmp/backup_pirate_usb.err"

# Parcourir les fichiers et les créer si nécessaire
for fichier in ${fichier_log} ${fichier_erreur}; do
  if [ -f ${fichier} ]; then
    rm -f ${fichier}
  else
    touch ${fichier}
  fi
done

# Fin des fichiers globaux
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fonctions globales
#
# -----------------------------------------------------------
## Fonction d'affichage en fonction du mode debug choisi
echod() { [ "$mode_debug" -ge 2 ] && echo "(DEBUG) $*" ; }
echok() { [ "$mode_debug" -ge 2 ] && echo "(ok) $*" ; }
echoi() { [ "$mode_debug" -ge 1 ] && echo "(ii) $*" ; }
echow() { [ "$mode_debug" -ge 1 ] && echo "(!!) $*" ; }
echnk() { [ "$mode_debug" -ge 0 ] && echo "(EE) $*" ; }

## Redirection de la sortie standard (stdout) vers le fichier de log
redirect_stdout()
{
  # Sauvegarder la valeur de stdout dans le descripteur de fichier 6
  exec 6>&1

  # stdout remplacé par le fichier de log
  exec > ${fichier_log}
}

## Restauration de stdout
restore_stdout()
{
  # Test si le descripteur de fichier 6 existe
  if [ "$(lsof -a -p $$ -d6 | grep 6)" ]; then
    echo "Restauration de stdout et fermeture du descripteur de fichier 6"
    # Restaurer stdout et fermer le descripteur de fichier #6
    exec 1>&6 6>&-
  else
    echo "Le descripteur de fichier 6 n'existe pas. stdout est normalement en place"
  fi

}

## Redirection de la sortie d'erreur standard (stderr) vers le fichier d'erreur
redirect_stderr()
{
  # Sauvegarder la valeur de stderr dans le descripteur de fichier 7
  exec 7>&2

  # stderr remplacé par le fichier d'erreur
  exec 2> ${fichier_erreur}
}

## Restauration de stderr
restore_stderr()
{
  # Test si le descripteur de fichier 7 existe
  if [ "$(lsof -a -p $$ -d7 | grep 7)" ]; then
    echo "Restauration de stderr et fermeture du descripteur de fichier 7"
    # Restaurer stderr et fermer le descripteur de fichier #7
    exec 2>&7 7>&-
  else
    echo "Le descripteur de fichier 7 n'existe pas. stderr est normalement en place"
  fi

}


## Fonction d'erreur
# Affiche le message relative à la position de l'erreur                                                                                                                 # Et affiche les erreurs renvoyées
# - Prototype : erreur "MESSAGE_ERREUR"
erreur()
{
  ## Vérification du code retour de la dernière commande
  case "$?" in
    0 ) ## Si 0, tout s'est bien passé
      > "${fichier_erreur}" ## On vide le fichier d'erreur
      ;;  ## Pas d'erreur
    * ) ## Si différent de 0
      ## On affiche le message relatif à l'erreur
      echnk "Erreur lors de: -> ${1} <-"
      ## Et on arrête le script avec un code retour 1
      echnk "/!\\ Arrêt du script /!\\"
      exit 1;
      ;;
  esac
}

# Fin des fonctions globales
# -----------------------------------------------------------
# **********************************************************************************************



# **********************************************************************************************
#
# Programme principale
#
# -----------------------------------------------------------

# Vérification du nombre de paramètres, pas besoin de paramètres actuellement

echo "test no redirect 1 "

redirect_stdout
redirect_stderr

echo "Petit sleep de 10 secondes =)"
sleep 10s

echo "test redirect 2 "

# Création du point de montage si il n'existe pas
if [ ! -d $MOUNT_POINT ]; then
  echo "Création du point de montage"
  mkdir -p $MOUNT_POINT
fi

# Montage de la partition
echo "Montage de la partition"
mount $PART $MOUNT_POINT
# On vérifie si le montage s'est bien effectué
erreur "Montage de la partition"


# Création du répertoire de sauvegarde sur le point de montage
if [ ! -d $MOUNT_POINT/$BKP_DIR ]; then
  echo "Création du répertoire de sauvegarde sur le point de montage"
  mkdir -p $MOUNT_POINT/$BKP_DIR
fi

# Commande de synchronisation
echo "Début synchronisation"
rsync -rl --delete --stats $DIR $MOUNT_POINT/$BKP_DIR
#rsync -rl --delete --stats $DIR $MOUNT_POINT/$BKP_DIR > ${fichier_log} 2> ${fichier_erreur}

# Commande de synchronisation avec affichage graphique de la progression
#rsync -rl --delete --stats $DIR $MOUNT_POINT/$BKP_DIR  | zenity --text-info --title="Updating Podcasts" --width=600 --height=600
echo "Fin synchronisation"

# On donne tous les droits sur les fichiers de logs
echo "Attribution des droits sur les fichiers de log"
chmod 777 ${fichier_log} ${fichier_erreur}

# Démontage de la partition
echo "Démontage de la partition"
umount $MOUNT_POINT


echo "test final redirect 3 "

echo "Restauration des sorties"
restore_stdout
restore_stderr

exit 0
# Fin de la boucle principale
# -----------------------------------------------------------
# **********************************************************************************************


