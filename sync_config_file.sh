#!/bin/sh
################################################################################################
## 
##  Nom: sync_config_file.sh 
##
##  Licence: Creative Commons
##
##  Comportement: Synchronise différents fichiers/dossiers de configuration dans un répertoire qui fait office de sauvegarde
##
##
##
################################################################################################
##
##  Date de création: 20/10/2010
##
##  Auteur: Gardouille
##
##
##  Dernière modifications:
##  20/10/2010: - Création
# **********************************************************************************************

##  2011/09/17 - 
##    - Ajout du fichier des applications d'ouverture par défauts pour les fichiers
## 

##  16/12/2010: - Ajout de la configuration de parcellite
##  21/12/2010: - Ajout de la configuration de screen - panel gnome 
##  16/02/2011: - Ajout de la configuration de teamviewer
##  27/04/2011: - Ajout de la configuration de moc
##  18/08/2011: - Ajotu de la configuration de samba
##  30/09/2011: - Ajout de la configuration de remmina minecraft
# **********************************************************************************************
##
##  À modifier:
##    - Ajouter la configuration de freeplane
##    - Faire une nouvelle configuration pour gedit
##    - Trouver tout les bons droits sur les fichiers à restaurer
##    - Possibilité de sauvegarder la configuration de gedit avec gconftool? NON
##
################################################################################################



# **********************************************************************************************
# 
# Variables globales
# 
# -----------------------------------------------------------
# Répertoire personnel de l'utilisateur:
USER_DIR="/home/darker"

# Fin des variables globales
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
# 
# Fichiers globaux
# 
# -----------------------------------------------------------
# 

# Fin des fichierss globaux
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
# 
# Fonctions globales
# 
# -----------------------------------------------------------
# Divers echos ...
echoi() { echo " (ii) $*" >> "${log}" ; }
echok() { echo " (ok) $*" >> "${log}" ; }
echow() { echo " (!!) $*" >> "${log}" ; }
echnk() { echo " (EE) $*" >> "${log}" ; }

# Fin des fonctions globales
# -----------------------------------------------------------
# **********************************************************************************************



# **********************************************************************************************
# 
# Programme principale
# 
# -----------------------------------------------------------

case "$1" in
  backup)

    if [ "${USER}" = "root" ]; then
      # Synchronisation des fichiers de configuration de zsh
      ## Droits:
      ## 644: /etc/zsh/*
      rsync -a --delete /etc/zsh /media/data/config_debian/etc/
      rsync -a /etc/dir_colors /media/data/config_debian/etc/

      # Synchronisation du fichier hosts
      ## Droits:
      ## 644: /etc/hosts
      rsync -a /etc/hosts /media/data/config_debian/etc

      # Synchronisation des fichiers de configuration de aptitude
      ## Droits:
      ## 644: /etc/apt/{preferences,sources.list}
      rsync -a --delete /etc/apt/sources.list /media/data/config_debian/etc/apt
      rsync -a --delete /etc/apt/preferences /media/data/config_debian/etc/apt

      # Copie du fichier fstab
      ## Droits:
      ## 644: /etc/fstab
      cp /etc/fstab /media/data/config_debian/etc/fstab.backup
  
      # Synchronisation du répertoire samba
      ## Droits:
      ## 644: /etc/samba/* (récursif pour tous les fichiers)
      rsync -a --delete /etc/samba /media/data/config_debian/etc/

      # Fichier dictionnaire de vim
      ## Droits:
      ## 644: /usr/share/vim/vim73/spell/* 
#      rsync -a --delete /usr/share/vim/vim73/spell /media/data/config_debian/usr/share/vim/vim73

      # Configuration des interfaces réseaux
      ## Droits: 
      ## 644: /etc/network/interfaces
      rsync -a --delete /etc/network/interfaces /media/data/config_debian/etc/network/
    else
      #gconftool-2 --dump /apps/panel > /media/data/config_debian/home/my-panel-setup.entries
      echo "pas de sauvegarde gconftool pour le moment"
    fi

    ## Répertoire personnel
    # Synchro du fichier de config de terminator
    ## Droits:
    ## 644: ~/.config/terminator/config
    rsync -a --delete "${USER_DIR}"/.config/terminator/config /media/data/config_debian/home/.config/terminator

    # Modèles de documents
    ## 644: ~/.config/user-dirs.dirs ~/Modeles/*
    rsync -a --delete "${USER_DIR}"/.config/user-dirs.dirs /media/data/config_debian/home/.config/
    rsync -a --delete "${USER_DIR}"/Modeles /media/data/config_debian/home/

    # Configuration de pidgin (comptes et historique)
    ## Droits:
    ## 700: .purple
    ## 600: .purple/*.xml (récursif)
    ## 644: .purple/*.html (récursif) (historique en lecture pour tout le monde ... ?)
    rsync -a --delete "${USER_DIR}"/.purple /media/data/config_debian/home

    # Configuration de mozilla
    ## Droits: 
    ## 700: .mozilla (récursif pour les dossiers)
    # rsync -a "${USER_DIR}"/.mozilla/firefox/xb6j4gn3.default/ /media/data/config_debian/home/.mozilla/firefox/
    rsync -a "${USER_DIR}"/.mozilla/firefox/profiles.ini /media/data/config_debian/home/.mozilla/firefox

    # configuration de icedove sans les mails imap (gmail, hotmail, mailoo)
    ## Droits: 
    ## 700: .icedove (récursif pour les dossiers)
    rsync -a --delete --filter "- ImapMail/" "${USER_DIR}"/.icedove /media/data/config_debian/home/

    # configuration de skype
    rsync -a --delete "${USER_DIR}"/.Skype /media/data/config_debian/home

    # Configuration de Libreoffice (templates, dico, ...)
    ## Droits:
    ## 755: .libreoffice (récursif pour les dossiers)
    rsync -a --delete "${USER_DIR}"/.libreoffice /media/data/config_debian/home

    # Configuration de vim
    ## 700: .vim (récursif pour les dossiers)
    ## 640: pour les fichiers
    rsync -a --delete "${USER_DIR}"/.vim* /media/data/config_debian/home

    # configuration de ssh (nom d'utilisateur suivant l'host)
    ## 700: .ssh
    ## 600: .ssh/*
    rsync -a --delete "${USER_DIR}"/.ssh/config /media/data/config_debian/home/.ssh

    # Configuration de Eye Of Gnome (eog), plugin activés, ...
    ## -> Pas de fichier de config <-
#    rsync -a --delete "${USER_DIR}"/.gconf/apps/eog/plugins/%gconf.xml /media/data/config_debian/home/.gconf/apps/eog/plugins

    # configuration de gedit
    ## -> plus de fichier de configuration à propremement parler.
#    rsync -a --delete "${USER_DIR}"/.gconf/apps/gedit-2 /media/data/config_debian/home/.gconf/apps

    # configuration de gnome
#    rsync -a --delete "${USER_DIR}"/.gconf/apps/panel /media/data/config_debian/home/.gconf/apps
#    rsync -a --delete "${USER_DIR}"/.gconf/desktop/gnome /media/data/config_debian/home/.gconf/desktop

    # Script nautilus
    ## Recréer les liens:
    # Resize Image (1024px) -> ../nautilus-resize-image/ResizeImage.sh
    # Resize Image (800px) -> ../nautilus-resize-image/ResizeImage.sh
    ## 755: ~/.gnome2/nautilus-scripts/*
    rsync -a --delete "${USER_DIR}"/.gnome2/nautilus-* /media/data/config_debian/home/.gnome2/

    # script
    ## 755: ~/bin/*
    rsync -a --delete "${USER_DIR}"/bin /media/data/config_debian/home/

    # configuration de irssi
    ## 755: ~/.irssi/*
    ## 640: ~/.irssi/config 
    rsync -a --delete "${USER_DIR}"/.irssi /media/data/config_debian/home/

    # icônes
    ## Droits:
    ## 777: .icons/*
    rsync -a --delete "${USER_DIR}"/.icons /media/data/config_debian/home/

    # configuration et bibliothèque de rhythmbox (note des chansons!)
    ## Droits: 
    ## 700: .local/share/rhythmbox
    ## 644: .local/share/rhythmbox/*
    rsync -a --delete "${USER_DIR}"/.local/share/rhythmbox /media/data/config_debian/home/.local/share/
    ## 700: .gconf/apps/rhythmbox
    ## 600: .gconf/apps/rhythmbox/*.xml (récursif)
    rsync -a --delete "${USER_DIR}"/.gconf/apps/rhythmbox /media/data/config_debian/home/.gconf/apps/
#    rsync -a --delete /usr/lib/rhythmbox/plugins/vkontakte /media/data/config_debian/usr/lib/rhythmbox/plugins

    # Configuration de filezilla
    ## 644 ~/.filezilla/*
    rsync -a --delete "${USER_DIR}"/.filezilla /media/data/config_debian/home

    # Configuration de l'applet audio
    rsync -a --delete "${USER_DIR}"/.gstreamer-0.10 /media/data/config_debian/home

    # Configuration de TSClient
    ## 644: ~/.tsclient/*
    rsync -a --delete "${USER_DIR}"/.tsclient /media/data/config_debian/home

    # Configuration de Transmission
    ## 755: .config/transmission/* (récursif pour les dossiers)
    ## 600: .config/transmission/* (récursif pour les fichiers)
    rsync -a --delete "${USER_DIR}"/.config/transmission /media/data/config_debian/home/.config

    # Configuration de l'écran de veille
    ## 

    # Configuration de Parcellite
    rsync -a --delete "${USER_DIR}"/.config/parcellite /media/data/config_debian/home/.config

    # Configuration de screen
    ## 600: .screen/*
    rsync -a --delete "${USER_DIR}"/.screen /media/data/config_debian/home/

    # Configuration de teamviewer
    ## 755: ~/.teamviewer
    ## NE PAS RESTAURER TOUT LE RÉPERTOIRE TEAMVIEWER -> Trouver le fichier de config
    rsync -a --delete "${USER_DIR}"/.teamviewer /media/data/config_debian/home/

    # Configuration de moc (music on console)
    ## 
    rsync -a --delete "${USER_DIR}"/.moc /media/data/config_debian/home/

    # Configuration des programmes par défaut pour ouvrir les fichiers:
    ## 644: .local/share/applications/mimeapps.list
    rsync -a --delete "${USER_DIR}"/.local/share/applications/mimeapps.list /media/data/config_debian/home/.local/share/applications/

    # Configuration de remmina
    ## 755: .remmina
    ## 644: .remmina/*
    rsync -a --delete ~/.remmina /media/data/config_debian/home/

    # Configuration/save de minecraft
    ## 755: ~/.minecraft (récursif sur les répertoires)
    ## 777 en récursif sur les fichiers?
    rsync -a --delete ~/.minecraft /media/data/config_debian/home/

    ;;


  restore)
    ## Restoration /etc/
    if [ "${USER}" = "root" ]; then
      rm -rf /etc/zsh/ 
      rm -rf /etc/hosts
      rsync -a /media/data/config_debian/etc/ /etc
      rsync -a /media/data/config_debian/usr/ /usr

      exit 0
    else
#      gconftool-2 --load /media/data/config_debian/home/my-panel-setup.entries
      echo "pas de gconftool pour le moment"
    fi 

    ## Répertoire personnel
    rsync -a /media/data/config_debian/home/ "${USER_DIR}"

    ## Attribution des droits sur "${USER_DIR}"
    cd "${USER_DIR}"
    find . -type d -exec chmod 700 {} \;
    chmod -R 755 bin/
    chmod -R 700 .ssh/
    chown -R darker:darker "${USER_DIR}"

    ## Attribution des droits sur ~/.freeplane:
    chmod 644 "${USER_DIR}"/.freeplane/
    ;;
  *)
    echo "Utilisation: $0 [backup|restore]"
    exit 1
esac

exit 0

