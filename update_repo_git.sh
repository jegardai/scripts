#!/bin/bash

update() {
  REPO_URL="${1}"
  REPO_PATH="${2}"

  `ping -q -c 3 "${REPO_URL}" > /dev/null 2>&1`

  # If $REPO_URL is available
  if [ $? -eq 0 ]; then
    echo "${REPO_URL} is available"
    pushd "${REPO_PATH}" > /dev/null 2>&1
    git pull > /dev/null 2>&1
    popd > /dev/null 2>&1
  else
    echo "${REPO_URL} is not available"
  fi
}

# Update Puppet IPR repo
update "git1.ipr.univ-rennes1.fr" "/home/jegardai/repos/ipr_puppet"
# Update Gardouille www data (dokuwiki, shaarli, respawn, ...)
update "www.google.fr" "/home/jegardai/repos/gardouille_www"

