#!/bin/sh
# .. vim: foldmarker=[[[,]]]:foldmethod=marker

# This script will try to:
## Get first argument
## Or get clipboard content if no argument
## Send this data to an URL shortener
## Print the short url to stdout
## Or put it in the clipboard if no argument was given

# Vars [[[
debug=false
flag_clipboard=false
flag_inline=false

null_service_url="https://null.101010.fr"

## Colors [[[
c_redb='\033[1;31m'
c_magentab='\033[1;35m'
c_reset='\033[0m'
## ]]]

# ]]]

# Function to print a debug message [[[
debug_message() {
	_message="${1}"

	[ "${debug}" = "true" ] && printf "${c_magentab}%-6b${c_reset}\n" "DEBUG ${_message}"
}
# ]]]

# Verify argument [[[
case "$#" in
	0 )
		flag_clipboard=true
		debug_message "− Verify arg : No argument was given, try to use clipboard."
		;;
	1 )
		flag_inline=true
		debug_message "− Verify arg : One argument was given, use it."
		;;
	* )
		printf "${c_redb}%b${c_reset}\n" "Error : Expect one argument or a content in clipboard."
		exit 1
		;;
esac
# ]]]

# Get URL to be shortened [[[
# Try to get URL from first argument
if [ "${flag_inline}" = "true" ]; then
	url_to_short="${1}"
fi
# Try to get URL from clipboard
if [ "${flag_clipboard}" = "true" ]; then
	url_to_short=$(xclip -out -selection clipboard)
fi

debug_message "− Get URL : URL to be shortened : ${url_to_short}"
# ]]]
# Ensure the URL wasn't already shortened [[[
if printf -- '%s' "${url_to_short}" | grep -q -E -- "${null_service_url}"
then
	printf "${c_redb}%b${c_reset}\n" "Error : The url to be shortened (${url_to_short}) already seems to comes from the 0x0 service (${null_service_url})."
	exit 1
fi
# ]]]

# shorten URL
result=$(curl -sF"shorten=${url_to_short}" "${null_service_url}")

# Manage the result [[[
## If the URL should simply be printed to stdout
if [ "${flag_inline}" = "true" ]; then
	debug_message "− Manage result : Print the result on stdout :"
	printf "%b\n" "${result}"
fi
# If the URL should remplace the previous content of the clipboard
if [ "${flag_clipboard}" = "true" ]; then
	debug_message "− Manage result : Put the result in clipboard."
	echo "${result}" | xclip -rmlastnl -selection clipboard
fi

# ]]]

exit 0
