#!/bin/sh

manga_title="area-d"
manga_path="./${manga_title}"
start_chapter=099
max_chapter=100
#start_chapter=1
#max_chapter=117
image_format="jpg"


for chapter in $(seq "${start_chapter}" "${max_chapter}"); do
  # Create the chapter directory
  mkdir -p "${manga_path}/${chapter}"
  pushd "${manga_path}/${chapter}"

  #
  image=001

  # Allow some download errors
  error=0
  max_error=5


  while [ $error -lt $max_error ]; do
    # Build the url to download image and download it
    url="http://www.lirescan.com/images/lecture-en-ligne/${manga_title}/${chapter}/${image}.${image_format}"
    printf "Download ${image} from chapter ${chapter}\n"
    wget ${url} --quiet

    if [ $? -eq 0 ]; then {
      #image=$((image++))
      #image=$(printf %02d $((image+1)) )
      #image=$((image+1))
      error=0
    }
    else {
      error=$((error+1))
    }
    fi
    # Next image (need to be 0x)
    image=$(printf "%02d" $((image+1)) )
    # If image need to be 00x …)
    #image=$(printf "%03d" $((image+1)) )

  done

  popd


done
