#!/bin/sh
localIp='192.168.2.10/24'
localBroadcast='192.168.2.255'
netAdp=$1
jollaIp='192.168.2.15'

sudo ip addr add $localIp broadcast $localBroadcast dev $netAdp
sudo ip route add default via $jollaIp dev $netAdp onlink
