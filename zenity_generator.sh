#!/bin/bash

##############################################
##############################################
############## ZENITY GENERATOR ##############
##############################################
#####################################by Hizoka
# Script de génération de fenêtres graphiques réalisé uniquement avec Zenity et du bash
# Pour des questions ou des infos : http://forum.ubuntu-fr.org/viewtopic.php?id=280649

### Fonctions courantes ###

# Fonction cherchant l'emplacement du script (pour le .log)
function emplacement_script()
{
if [ "${0:0:1}" == "/" ];
then
  emplacement_script=$(dirname "$0")
else
  emplacement_script=$(pwd)
fi
}

# Fonction affichant le code final de la nouvelle fenêtre créée avec copie dans le log si besoin
function affichage_code()
{
  zenity --text-info --title="Code de votre fenêtre :" --width="500" --height="400" --filename=".fenetre_zenity"

  if [[ -n $(egrep "^choix_log=OUI$" "${emplacement_script}/zenity_generator.sh") ]]
  then
    (cat .fenetre_zenity && echo -e "\n") >> "${emplacement_script}/log_zenity_generator.log"
  fi

  rm .fenetre_zenity
}

# Fonction donnant la hauteur de la fenêtre
function hauteur_fenetre()
{
  hauteur_fenetre="--height=\"$(zenity --width=300 --height=80 --title 'Choix de la hauteur' --text 'Indiquez la taille (en pixel) maximale de la hauteur de la fenêtre' --entry --width=400 --height=100)\""
}

# Fonction donnant l'Icône de la fenêtre
function icone_fenetre()
{
  icone_fenetre="--window-icon=\"$(zenity --title 'Choix de l`icône' --filename=/home/$USER/ --file-selection --width=400)\""
}

# Fonction donnant la largeur de la fenêtre
function largeur_fenetre()
{
  largeur_fenetre="--width=\"$(zenity --width=300 --height=80 --title 'Choix de la largeur' --text 'Indiquez la taille (en pixel) maximale de la largeur de la fenêtre' --entry --width=400 --height=100)\""
}

# Fonction donnant le texte avec couleur et gras
function texte()
{
echo -e "Vous pouvez maintenant taper le texte que vous souhaitez voir apparaître dans votre fenêtre.
Il est possible de faire des effets sur les polices, pour plus d'infos, regardez le menu 'exemples'.

Evitez les !!, cela provoque des erreurs.

Exemples de balises pour mettre en forme le texte interne de vos fenêtres (exceptées celles de saisie de texte) :
 - Mise en gras : <b> texte </b>
 - Mise en italique : <i> Texte </i>
 - Mise en couleur : <span color=\\\"red\\\"> texte </span> (avec les anti-slashs)

##### MERCI DE SUPPRIMER CES INFOS AVANT DE CLIQUER SUR FERMER #####" > .texte

texte="--text=\"$(zenity --text-info --title='Tapez le texte de la fenêtre' --editable --filename='.texte' --width='550' --height='500')\""

rm .texte
}

# Fonction donnant le titre
function titre()
{
  titre="--title=\"$(zenity --title 'Choix du titre' --text 'Tapez le nom de la fenêtre' --entry --width=400 --height=100)\""
}

# Fonction pour utilisation ou non d'un fichier log
function utilisation_log()
{
emplacement_script

if [[ -n $(egrep "^choix_log=OUI$" "${emplacement_script}/zenity_generator.sh") ]]
then
  echo "Vous enregistrez vos fenêtres dans un fichier log disponible dans le même dossier que ce script.

Pour arrêter d'utiliser le fichier log :
sed -i 's/^choix_log=OUI$/choix_log=NON/' \"${emplacement_script}/zenity_generator.sh\""

elif [[ -n $(egrep "^choix_log=NON$" "${emplacement_script}/zenity_generator.sh") ]]
then
  echo "Vous ne souhaitez pas enregistrer vos codes de fenêtres dans un fichier log.

Pour utiliser un fichier log de sauvegarde :
sed -i \"s/^choix_log=NON$/choix_log=OUI/\" \"${emplacement_script}/zenity_generator.sh\""

else
  log=$(zenity --list --radiolist --title="Utilisation d'un .log ?" --text="Voulez-vous que vos créations soient sauvegardées dans un fichier log ?

Cette question ne vous sera posée qu'une seule fois.

Il vous sera possible de modifier ce choix en tapant la commande
qui s'affiche dans la console au démarrage du script." --column="Choix" --column="Réponse" \
FALSE "OUI" \
FALSE "NON")

  if [[ -n ${log} ]]
  then
    sed -i "/^#### Choix log ####$/a choix_log=${log}" "${emplacement_script}/zenity_generator.sh"
  fi
fi

#### Choix log ####
choix_log=OUI

}


###############################################################
## Système pour la fenêtre Zenity affichant un fichier texte ##
###############################################################

# Permet également sauvegarder après.
function zenity_fichier_texte()
{
# Ouvre une fenêtre zenity demandant les differentes variables utilent à la creation de la fenêtre
MENU=$(zenity --title 'Que voulez vous modifier ?' --list --text 'Merci de choisir les différentes options à modifier ou à appliquer.' --checklist --column 'Choix' --column 'Action' --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du fichier texte à afficher" \
FALSE "Permettre l'édition du texte de la fenêtre" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre" )

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du fichier texte à afficher") ]]
then
  fichier_texte="--filename=\"$(zenity --title 'Emplacement du fichier texte à afficher dans la fenêtre.'  --filename=/home/$USER/ --file-selection)\""
fi

if [[ -n $(echo ${MENU} | grep "Permettre l'édition du texte de la fenêtre") ]]
then
  edition="--editable"
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --text-info ${titre} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${fichier_texte} ${edition}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}



###############################################
## Système pour la fenêtre Zenity calendrier ##
###############################################

# Fonction du format de la date
function date()
{
  date="--date-format=\"$(zenity --title 'Choix du format de la date' --text 'Choisissez le format de date voulu en vous aidant des explications.

Jours --
%d : Jour du mois [ex : 12]
%a : 3 premières lettres du jour [ex : ven]
%A : Affiche le nom du jour [ex : vendredi]
%j : Numéro du jour depuis le debut d`année [ex : 286]

Semaines --
%V : Numéro de lasemaine [ex : 52]

Mois --
%m : Mois de l`année [ex: 10]
%b : 3 premières lettres du mois [ex : oct]
%B : Affiche le nom du mois [ex : octobre]

Années --
%y : Année, sur 2 chiffres [ex : 84]
%Y : Année, sur 4 chiffres [ex : 1984]

Formats --
%D : mois/jour/année [ex : 07/23/08]
%F : année-mois-jour [ex : 2008-07-21]
%x : jour.mois.année [ex : 21.07.2008]
%c : %a %d %b %Y heure [ex : lun 21 jui 2008 00:00:00]

Exemple : Nous sommes le %A %d %m %Y' --entry --width=400 --height=100)\""
}

function zenity_calendrier()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
FALSE "Choix du jour de base" \
FALSE "Choix du mois de base" \
FALSE "Choix de l'année de base" \
FALSE "Choix du format de sorti de date" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Choix du jour de base") ]]
then
  jour="--day=\"$(zenity --title 'Choix du jour' --text 'Choisissez le jour de base.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Choix du mois de base") ]]
then
  mois="--month=\"$(zenity --title 'Choix du mois' --text 'Choisissez le mois de base.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Choix de l'année de base") ]]
then
  annee="--year=\"$(zenity --title 'Choix de l`année' --text 'Choisissez l`année.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Choix du format de sorti de date") ]]
then
  date
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --calendar ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${jour} ${mois} ${annee} ${date}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


#############################################
## Système pour la fenêtre Zenity messages ##
#############################################

# Fonction du type de fenêtre message
function type_fenetre()
{
type_fenetre=$(zenity --title="Quel type de fenêtre de message voulez-vous ?" --text="Veuillez choisir dans la liste suivante le type de fenêtre voulu." "Fenetre d'erreur" "Fenetre de confirmation" "Fenetre d'avertissement" --entry --entry-text="Fenetre d'information")

if [ "$type_fenetre" = "Fenetre d'information" ]
then
  type_fenetre="--info"

elif [ "$type_fenetre" = "Fenetre d'erreur" ]
then
  type_fenetre="--error"

elif [ "$type_fenetre" = "Fenetre de confirmation" ]
then
  type_fenetre="--question"

elif [ "$type_fenetre" = "Fenetre d'avertissement" ]
then
  type_fenetre="--warning"
fi
}

function zenity_messages()
{
MENU=$(zenity --title "Que voulez-vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
TRUE "Choix du type de message" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Choix du type de message") ]]
then
  type_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity ${type_fenetre} ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


####################################################################
## Système pour la fenêtre Zenity de sélection de fichier/dossier ##
####################################################################

function zenity_selection_fichier()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
FALSE "Désigner le dossier de base" \
FALSE "Sélectionner plusieurs fichiers" \
FALSE "Choix du séparateur -- si utilisation du mode multi fichier" \
FALSE "Mode enregistrement de fichier" \
FALSE "Obligation de sélection de dossiers et non de fichiers" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")


if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Designer le dossier de base") ]]
then
  emplacement_defaut="--filename=\"$(zenity --title 'Choix du dossier' --filename=/home/$USER/ --file-selection --directory --width=400)\""
fi

if [[ -n $(echo ${MENU} | grep "Selectionner plusieurs fichiers") ]]
then
  multi_fichier="--multiple"
fi

if [[ -n $(echo ${MENU} | grep "Choix du séparateur") ]]
then
  separateur_fichier="--separator=\"$(zenity --title 'Choix du separateur' --text 'Choisissez le type de séparateur à utiliser pour delimiter les différentes adresses.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Mode enregistrement de fichier") ]]
then
  sauvegarde="--save"
fi

if [[ -n $(echo ${MENU} | grep "Obligation de selection de dossiers et non de fichiers") ]]
then
  dossier="--directory"
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --file-selection ${titre} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${dossier} ${sauvegarde} ${separateur_fichier} ${multi_fichier} ${emplacement_defaut}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


########################################################
## Système pour la fenêtre Zenity de boite de réponse ##
########################################################

function zenity_saisie()
{
MENU=$(zenity --title "Que voulez-vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
FALSE "Pré-écrire une réponse dans la zone de texte" \
FALSE "Proposer une liste de réponses" \
FALSE "Remplacer les caracteres par des *" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Pré-écrire une réponse dans la zone de texte") ]]
then
  pre_texte="--entry-text=\"$(zenity --title 'Texte pré écrit' --text 'Indiquez le texte qui doit être pré écrit dans la barre de texte.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Remplacer les caracteres par des *") ]]
then
  motdepasse="--hide-text"
fi

if [[ -n $(echo ${MENU} | grep "Proposer une liste de réponses") ]]
then
  liste_reponse=$(zenity --title 'Liste de choix' --text 'Ecrivez les différents choix que vous souhaitez proposer.

Il suffit d`éspacer les différentes réponses.

Si votre réponse contient plusieurs mots, il suffit de mettre des " à l`expression.

Si vous avez mis une réponse pré-écrite, il est inutil de la remettre ici.' --entry --width=400 --height=100)
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --entry ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${pre_texte} ${motdepasse} ${liste_reponse}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


###########################################################
## Système pour la fenêtre Zenity de barre de chargement ##
###########################################################

function texte_et_pourcentage()
{
nb_etape=$(zenity --title "Nombre d'étape" --text "Indiquez le nombre d'étape à realiser pour remplir la barre." --entry --width=400 --height=100)

etapes_pourcentages=$[100 / $nb_etape]

zenity --question --title="Choisir les pourcentages ?" --text="Souhaitez vous choisir les différents pourcentages des ${nb_etape} étapes ?

Si vous ne souhaitez pas choisir, les étapes iront de ${etapes_pourcentages}% en ${etapes_pourcentages}%."

question=$(echo $?)

for ((i=1; i<=$nb_etape; i++));
do
  if [[ "${question}" == "1" ]]
  then
    pourcentage=$(zenity --title "Pourcentage de l'étape $i" --text "Indiquez le pourcentage n°$i" --entry)

  elif [[ "${question}" == "0" ]]
  then
    pourcentage=$(($etapes_pourcentages * $i))
  fi

  texte_pourcentage=$(zenity --title "Texte associé à ${pourcentage}%" --text "Indiquer le texte à afficher quand la barre arrive à ${pourcentage}%" --entry)

  if [[ -z ${texte_et_pourcentage} ]]
  then
    texte_et_pourcentage="echo \"# ${texte_pourcentage}\" ; sleep 1
echo \"${pourcentage}\" ; sleep 1"

  else
    texte_et_pourcentage="${texte_et_pourcentage}
echo \"# ${texte_pourcentage}\" ; sleep 1
echo \"${pourcentage}\" ; sleep 1"
  fi

  echo "${pourcentage}% a pour texte : ${texte_pourcentage}"
done
}

function zenity_barre_chargement()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
FALSE "Fermer automatiquement la fenêtre" \
FALSE "Créer une barre allant et venant sans pourcentage" \
FALSE "Nombres d'étapes avant d'atteindre 100%" \
FALSE "Pourcentage de départ" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
  texte_et_pourcentage="sleep $(zenity --entry --title='Combien de secondes avant le lancement ?' --text='Veuillez choisir le temps pendant lequel le texte de la fenêtre
doit être lisible avant que les textes de la barre n`apparraissent.') ${texte_et_pourcentage}"
fi

if [[ -n $(echo ${MENU} | grep "Fermer automatiquement la fenêtre") ]]
then
  autoclose="--auto-close"
fi

if [[ -n $(echo ${MENU} | grep "Créer une barre allant et venant sans pourcentage") ]]
then
  pulsate="--pulsate"
fi

if [[ -n $(echo ${MENU} | grep "Nombres d'étapes avant d'atteindre 100%") ]]
then
  texte_et_pourcentage
fi

if [[ -n $(echo ${MENU} | grep "Pourcentage de départ") ]]
then
  pourcentage_depart="--percentage=\"$(zenity --title 'Pourcentage de départ' --text 'Choisissez à combien de % la barre doit démarrer.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo "(${texte_et_pourcentage}) | zenity --progress  ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${pourcentage_depart} ${pulsate} ${autoclose}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


#############################################
## Système pour la fenêtre Zenity de liste ##
#############################################

function nb_colonne()
{
nb_colonne=$(zenity --title "Nombre de colonne" --text "Indiquez le nombre de colonne à mettre.

Dans le cas ou il y a des boutons/cases,
ajoutez une colonne pour leur emplacement." --entry --width=400 --height=100)

for ((i=1; i<=$nb_colonne; i++));
do
  colonne=$(zenity --title "Nom de la colonne n°$i" --text "Indiquez le nom que vous souhaitez attribuer à la colonne n°$i." --entry --width=400 --height=100)

  if [[ -z ${noms_colonnes} ]]
  then
    noms_colonnes="--column=\"${colonne}\""

  else
    noms_colonnes="${noms_colonnes} --column=\"${colonne}\""
  fi
done
}

function nb_reponses()
{
nb_reponses=$(zenity --title "Nombre de reponses" --text "Indiquez le nombre de reponses à proposer.

Cela équivaut au nombre de ligne que vous voulez proposer." --entry --width=400 --height=100)

for ((i=1; i<=$nb_reponses; i++));
do
  if [[ "${type_fenetre}" == "--radiolist" ]]
  then
    reponse=$(zenity --title "Réponse n°$i" --text "Tapez le texte que doit prendre la ligne n°$i.
Votre ligne se sépare en ${nb_colonne} parties (le nombre de colonne).

Dans le cas d'une liste de bouton de choix,
vous pouvez indiquer la réponse de base par un TRUE,
pour les autres il faut mettre FALSE.

Dans le cas de 3 colonnes, il faudrait une réponse du genre :
FALSE \"Carottes\" \"Legumes\"." --entry --width=400 --height=100)

  elif [[ "${type_fenetre}" == "--checklist" ]]
  then
    reponse=$(zenity --title "Réponse n°$i" --text "Tapez le texte que doit prendre la ligne n°$i.
Votre ligne se sépare en ${nb_colonne} parties (le nombre de colonne).

Dans le cas d'une liste avec cases à cocher,
vous pouvez indiquer les réponses de base par un TRUE,
pour les autres il faut mettre FALSE.

Dans le cas de 3 colonnes, il faudrait une réponse du genre :
FALSE \"Carottes\" \"Legumes\"." --entry --width=400 --height=100)

  elif [[ "${type_fenetre}" == "--multiple" ]]
  then
    reponse=$(zenity --title "Réponse n°$i" --text "Tapez le texte que doit prendre la ligne n°$i.
Votre ligne se sépare en ${nb_colonne} partie (le nombre de colonne).

Dans le cas de 3 colonnes, il faudrait une réponse du genre :
\"Carottes\" \"Legumes\" \"En soupe\"." --entry --width=400 --height=100)
  fi

  if [[ -z ${choix_reponses} ]]
  then
    choix_reponses="\\
${reponse}"

  else
    choix_reponses="${choix_reponses} \\
${reponse}"
  fi
done
}

function zenity_listes()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
TRUE "Choix du type de liste à utiliser" \
TRUE "Nombres de colonnes" \
TRUE "Réponses à proposer" \
FALSE "Cacher des colonnes" \
FALSE "Choisir la colonne a retourner par la fenêtre" \
FALSE "Choix du séparateur de réponse -- liste avec cases à cocher" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Choix du type de liste à utiliser") ]]
then
  type_fenetre=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --column "" --column "Type de liste" --hide-column="1" --width=400 --height=325 \
"radiolist" "Liste avec bouton de selection" \
"checklist" "Liste avec cases à cocher" \
"" "Liste simple" \
"multiple" "Liste simple avec selection multiple")

  if [[ -n ${type_fenetre} ]]
  then
    type_fenetre="--${type_fenetre}"
  fi
fi

if [[ -n $(echo ${MENU} | grep "Nombres de colonnes") ]]
then
  nb_colonne
fi

if [[ -n $(echo ${MENU} | grep "Réponses à proposer") ]]
then
  nb_reponses
fi

if [[ -n $(echo ${MENU} | grep "Cacher des colonnes") ]]
then
  colonne_fantome="--hide-column=\"$(zenity --title 'Colonne fantome' --text 'Indiquez le numéro de la colonne à cacher.

Si vous en cachez plusieurs, séparez les chiffres d`une virgule.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Choisir la colonne a retourner par la fenêtre") ]]
then
  colonne_retour="--print-column=\"$(zenity --title 'Colonne à retourner' --text 'L`interet de ce Système est de choisir quelle colonne
sera renvoyée par la fenêtre zenity apres la réponse,
c`est à dire ce que retourne : echo "$varible-de-la-fenetre".

Indiquez le numéro de la colonne à retourner.

Vous pouvez en retourner plusieurs en séparant les chiffres par des virgules.

Vous pouvez également choisir ALL afin de retourner toutes les colonnes.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Choix du séparateur de réponse -- liste avec cases à cocher") ]]
then
  separateur_fichier="--separator=\"$(zenity --title 'Choix du separateur' --text 'Choisissez le type de séparateur à utiliser pour delimiter les différentes adresses.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --list ${type_fenetre} ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${noms_colonnes} ${colonne_fantome} ${colonne_retour} ${separateur_fichier} ${choix_reponses}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


####################################################
## Système pour la fenêtre Zenity de notification ##
####################################################

function zenity_notification()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du texte à afficher dans la fenêtre" \
TRUE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --notification ${texte} ${icone_fenetre}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code
else
  menu
fi
}


#################################################
## Système pour la fenêtre Zenity avec curseur ##
#################################################

function zenity_curseur()
{
MENU=$(zenity --title "Que voulez vous modifier ?" --list --text "Merci de choisir les différentes options à modifier ou à appliquer." --checklist --column "Choix" --column "Action" --width=500 --height=400 \
TRUE "Choix du titre de la fenêtre" \
TRUE "Choix du texte à afficher dans la fenêtre" \
FALSE "Valeur d'incrémentation" \
FALSE "Valeur de base" \
FALSE "Valeur minimale" \
FALSE "Valeur maximale" \
FALSE "Largeur maximale de la fenêtre" \
FALSE "Hauteur maximale de la fenêtre" \
FALSE "Icône de la fenêtre")

if [[ -n $(echo ${MENU} | grep "Choix du titre de la fenêtre") ]]
then
  titre
fi

if [[ -n $(echo ${MENU} | grep "Choix du texte à afficher dans la fenêtre") ]]
then
  texte
fi

if [[ -n $(echo ${MENU} | grep "Valeur d'incrémentation") ]]
then
  valeur_evolution="--step=\"$(zenity --title 'Choix de l`incrémentation' --text 'Choisissez de combien en combien doit avancer le curseur.

Ex : 2 = 2, 4, 6, 8...
3 = 3, 6, 9...' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Valeur de base") ]]
then
  valeur_base="--value=\"$(zenity --title 'Choix du chiffre de base' --text 'Choisissez le chiffre sur lequel le curseur doit être de base.' --entry --width=400 --height=100)\""
fi

if [[ -n $(echo ${MENU} | grep "Valeur minimale") ]]
then
valeur_minimale="--min-value=\"$(zenity --title 'Choix de la valeur minimale' --text 'Choisissez la valeur minimale pouvant être proposé par le curseur.' --entry --width=400 --height=100)\""

  if [[ -z "${valeur_base}" ]]
  then
    valeur_base="--value=\"$(zenity --title 'Choix du chiffre de base' --text 'Vous avez détérminer une valeur minimale,
il faut obligatoirement choisir le chiffre sur lequel le curseur doit être placé de base.
Celui-ci doit impérativement être supérieur à votre valeur minimale.' --entry --width=400 --height=100)\""
  fi

  verif_valeur_base=$(echo "${valeur_base}" | sed 's/--value=// ; s/"//g')
  verif_valeur_minimale=$(echo "${valeur_minimale}" | sed 's/--min-value=// ; s/"//g')
  if [[ ${verif_valeur_base} -lt ${verif_valeur_minimale} ]]
  then
    zenity --info --width=400 --height=100 --title "Erreur dans les chiffres" --text "Vous avez choisi une valeur minimale plus petite que la valeur de départ. Le code ne fonctionnera pas."
  fi
fi

if [[ -n $(echo ${MENU} | grep "Valeur maximale") ]]
then
  valeur_maximale="--max-value=\"$(zenity --title 'Choix de la valeur maximale' --text 'Choisissez la valeur maximale pouvant être proposé par le curseur.' --entry --width=400 --height=100)\""

  verif_valeur_evolution=$(echo "${valeur_evolution}" | sed 's/--step=// ; s/"//g')
  verif_valeur_maximale=$(echo "${valeur_maximale}" | sed 's/--max-value=// ; s/"//g')
  if [[ -n "${valeur_evolution}" && -n "${valeur_maximale}" && ${verif_valeur_maximale} -lt ${verif_valeur_evolution} ]]
  then
    zenity --info --width=400 --height=100 --title "Erreur dans les chiffres" --text "Vous avez selectionné une incrémentation supérieur à la limite haute, cela aura pour effet de passer directement à votre valeur maximale et non à votre valeur d'incrémentation."
  fi
fi

if [[ -n $(echo ${MENU} | grep "Largeur maximale de la fenêtre") ]]
then
  largeur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Hauteur maximale de la fenêtre") ]]
then
  hauteur_fenetre
fi

if [[ -n $(echo ${MENU} | grep "Icône de la fenêtre") ]]
then
  icone_fenetre
fi

if [[ -n ${MENU} ]]
then
  # Création du fichier texte qui servira à afficher le code final
  echo -e "zenity --scale ${titre} ${texte} ${largeur_fenetre} ${hauteur_fenetre} ${icone_fenetre} ${valeur_evolution} ${valeur_base} ${valeur_minimale} ${valeur_maximale}" | sed "s/  */ /g" > .fenetre_zenity

  affichage_code

else
  menu
fi
}


##############
## Exemples ##
##############

function exemples()
{
echo -e "Voici des codes d'exemples de fenêtres zenity.
Pour tester ces codes, il vous suffit de les coller dans votre console.

Voici d'abord des exemples sur comment mettre un peu de style à vos textes (balise \"text=\"),
il est bien entendu possible de combiner les différents effets entre eux...

###### Le texte à copier débute juste après cette ligne ######
zenity --info --text \"
<span font-family=\\\"Arial\\\">essai de polices</span>
<span font-family=\\\"sans-serif\\\">essai de polices</span>
<span font-family=\\\"Helvetica\\\">essai de polices</span>
<span font-family=\\\"impact\\\">essai de polices</span>
<span font-family=\\\"sans\\\">essai de polices</span>
<span font-family=\\\"webdings\\\">essai de polices</span>

<span color=\\\"red\\\">red</span>
<span color=\\\"green\\\">green</span>
<span color=\\\"blue\\\">blue</span>
<span color=\\\"yellow\\\">yellow</span>
<span color=\\\"magenta\\\">magenta</span>
<span color=\\\"white\\\">white</span>
<span color=\\\"black\\\">black</span>
<span color=\\\"gray\\\">gray</span>
<span color=\\\"lightblue\\\">lightblue</span>
<span color=\\\"lightgray\\\">lightgray</span>

<b>style</b>
<big>style</big>
<i>style</i>
<s>style</s>
<sub>style</sub>
<sup>style</sup>
<small>style</small>
<tt>style</tt>
<u>style</u>\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre avec une barre de chargement :
###### Le texte à copier débute juste après cette ligne ######
(sleep 2
echo \"# Nous attaquons les 20%\" ; sleep 1
echo \"20\" ; sleep 1
echo \"# Aller on continue jusqu'a 40%\" ; sleep 1
echo \"40\" ; sleep 1
echo \"# on est parti pour depasser la moitié\" ; sleep 1
echo \"60\" ; sleep 1
echo \"# Bientot fini...\" ; sleep 1
echo \"80\" ; sleep 1
echo \"# J'en vois le bout.\" ; sleep 1
echo \"100\" ; sleep 1) | zenity --progress --pulsate --title=\"Chargement\" --percentage \"5\" --text=\"On commence à 5%\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre avec un calendrier intégré :
###### Le texte à copier débute juste après cette ligne ######
zenity --calendar --title=\"Exemple avec calendrier\" --text=\"Veuillez indiquer votre date de naissance. Merci.\" --day=\"12\" --month=\"10\" --year=\"1984\" --date-format=\"%x\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre avec un curseur de séléction :
###### Le texte à copier débute juste après cette ligne ######
zenity --scale --title=\"Température actuelle\" --text=\"Veuillez indiquer la température actuelle de votre piece.\" --step=\"2\" --value=\"15\" --min-value=\"-10\" --max-value=\"40\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre de lecture de fichier texte :
###### Le texte à copier débute juste après cette ligne ######
zenity --text-info --title=\"Voici le code de ce script\" --filename=\"${emplacement_script}/zenity_generator.sh\" --width=\"500\" --height=\"400\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre de QCM :
###### Le texte à copier débute juste après cette ligne ######
zenity --list --checklist --title=\"Perso préféré\" --text=\"Veuillez indiquer le personnage que vous préférez parmis cette liste.
Vous pouvez choisir plusieurs reponses.\" --column=\"Oui\" --column=\"Nom\" --column=\"Age\" --separator=\"@\" --height=\"300\" \\\

FALSE \"Bender\" \"3ans\" \\\

TRUE \"Zoidberg\" \"Mort\" \\\

FALSE \"Fry\" \"32ans\" \\\

TRUE \"Leela\" \"Inconnu\" \\\

False \"Hermes\" \"On s'en fiche\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre demandant une confirmation :
###### Le texte à copier débute juste après cette ligne ######
zenity --question --title=\"Etes vous sûr de vous ?\" --text=\"etes vous totalement certain d'etre sur de ce que vous voulez faire ?

Merci de confirmer.

M.Windows Vista.\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre de notification :
Pour lancer une commande au clic sur la notification, il faut ajouter && suivi de votre commande.
###### Le texte à copier débute juste après cette ligne ######
zenity --notification --text=\"Veuillez mettre a jour le script.\" && /usr/bin/adept_updater
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre de saisie de texte :
###### Le texte à copier débute juste après cette ligne ######
zenity --entry --title=\"Ville préférée\" --text=\"Merci d'indiquer la ville qui vous tient à coeur afin de faire un sondage mondial.\" --entry-text=\"Caen\"
###### Le texte à copier ce termine juste avant cette ligne ######


Code d'une fenetre de séléction de fichier :
###### Le texte à copier débute juste après cette ligne ######
zenity --file-selection --title=\"Indiquer votre dossier personnel\" --directory --filename=\"~/\"
###### Le texte à copier ce termine juste avant cette ligne ######
" > .exemples

zenity --text-info --title="Exemples de codes :" --width="760" --height="550" --filename=".exemples"

rm .exemples
}


########################
## MENU PRINCIPAL !!! ##
########################

function menu()
{
MENU=$(zenity --title "Quel type de fenêtre voulez-vous créer ?" --list --text "Séléctionnez le type de fenêtre zenity que vous souhaitez créer parmis la liste suivante." --column="reponse n°" --hide-column="1" --column "Type de boite" --column "Aide" --width=700 --height=570 \
"01" "Barre de chargement" "
Barre de progression avec textes et pourcentages." \
"02" "Calendrier" "
Choix d'une date sur un calendrier." \
"03" "Curseur de placement" "
Choix d'un chiffre en utilisant un curseur amovible." \
"04" "Messages et confirmations" "
Boite de texte indiquant une erreur, une information,
une alerte ou une confirmation." \
"05" "Notification" "
Affiche une Icône et du texte dans la zone de notification." \
"06" "Séléction de fichier/dossier              " "
Choix d'un fichier ou d'un emplacement." \
"07" "Saisie de texte" "
Permet la saisie de caractères." \
"08" "Affichage de fichier texte" "
Affiche le contenu d'un fichier texte." \
"09" "Listes" "
Boite du genre question à choix unique ou multiple." \
"10" "Exemples de codes" "
Exemples de fenêtres et explications
sur les styles applicable aux textes." \
"11" "Réinitialiser l'utilisation du fichier log" "
Repose la question de l'utilisation d'un fichier log de save")


if [[ "$?" == "0" && -z ${MENU} ]]
then
  menu

elif [[ -n $(echo ${MENU} | grep "01") ]]
then
  zenity_barre_chargement

elif [[ -n $(echo ${MENU} | grep "02") ]]
then
  zenity_calendrier

elif [[ -n $(echo ${MENU} | grep "03") ]]
then
  zenity_curseur

elif [[ -n $(echo ${MENU} | grep "04") ]]
then
  zenity_messages

elif [[ -n $(echo ${MENU} | grep "05") ]]
then
  zenity_notification

elif [[ -n $(echo ${MENU} | grep "06") ]]
then
  zenity_selection_fichier

elif [[ -n $(echo ${MENU} | grep "07") ]]
then
  zenity_saisie

elif [[ -n $(echo ${MENU} | grep "08") ]]
then
  zenity_fichier_texte

elif [[ -n $(echo ${MENU} | grep "09") ]]
then
  zenity_listes

elif [[ -n $(echo ${MENU} | grep "10") ]]
then
  exemples
  menu

elif [[ -n $(echo ${MENU} | grep "11") ]]
then
  emplacement_script
  sed -i "/^choix_log=.*/d" "${emplacement_script}/zenity_generator.sh"
  menu
fi
}

utilisation_log

menu
