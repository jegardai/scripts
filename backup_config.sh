#!/bin/sh
################################################################################################
##
##  Nom: backup_config.sh
##
##  Version: 0.4
##
##  Licence: Creative Commons
##
##  Comportement: Sauvegarde l'ensemble des éléments de configuration de mon système Debian
##  Le script peut-être exécuté en root ou en utilisateur principal. Le premier permettra
##    de sauvegarder certains fichiers dont lui seul a l'accès. Et le second permettra de
##    sauvegarder uniquement les fichiers de son répertoire utilisateur.
##  Deux modes d'exécution sont également disponible:
##    - backup: pour sauvegarder les fichiers en fonction de l'utilisateur
##    - restore: pour restaurer les fichiers en fonction de l'utilisateur
##
##
##
##
################################################################################################
##
##  Date de création: 28-12-2011
##
##  Auteur: Gardouille
##
##
##  Dernière modifications:
##  28-12-2011 - 0.1:
##    - Création
# **********************************************************************************************
##  2012/02/28 - 0.5.1:
##    - Ajout de la configuration de tmux
##    - Ajout de la sauvegarde des icônes
##  2012/01/12 - 0.4:
##    - Ajout de la sauvegarde de la configuration de tint2
##    - Ajout de la sauvegarde de la configuration de gsimplecal (calendrier graphique tout simple)
##    - Ajout de la sauvegarde de la configuration de roxterm
##      - Pour le moment, roxterm est uniquement utilisé avec dmenu faute de pouvoir utiliser la touche "tab" dans les raccourcis clavier.
##    - Ajout de la sauvegarde d'Openbox
##  2012/01/11 - 0.3:
##    - Ajout de la sauvegarde du script pour lancer dmenu
##  2012/01/05 - 0.2:
##    - Ajout de la sauvegarde du fichier principal de devtodo
##    - Ajout de la sauvegarde du répertoire personnel pour le jeu vvvvvv
##
# **********************************************************************************************
##  À Modifier:
##    - Ajouter le mode restore
##    - La config dmenu via le Alt+F3
##    - Ajouter la config de fbpanel
##
##
################################################################################################



# **********************************************************************************************
#
# Variables globales
#
# -----------------------------------------------------------
# Répertoire de l'utilisateur principal
USER_DIR="/home/$USER"

# Emplacement de stockage des fichiers des fichiers systèmes à sauvegarder
#BKP_DIR="/media/data/test_backup"
BKP_DIR="/media/backup/config_file-`date +%y-%m-%d`"

# Emplacement de stockage des fichiers de l'utilisateur
#BKP_DIR_USER="/media/data/test_backup/home"
BKP_DIR_USER="/media/backup/config_file-`date +%y-%m-%d`/home"

## Choix du mode d'exécution du script:
## Niveau 0 (mode_debug=0)
# Exécution automatique avec normalement aucune interaction avec l'utilisateur.
## Niveau 1 (mode_debug=1)
# ...
## Niveau 2 (mode_debug=2)
# L'installation n'est pas en automatique (aptitude, ...)
# La sortie des différentes commandes est envoyée sur la sortie standard
mode_debug=2

case "$mode_debug" in
  0) ## Niveau minimum
    # La sortie standard sera redirigée dans un fichier de log
    sortie="> /dev/null 2> \"${fichier_erreur}\""
    ;;
  1) ## Niveau moyen
    # La sortie standard sera quand même affichée
    sortie="2> \"${fichier_erreur}\""
    ;;
  2) ## Niveau maximum
    # Le résultat des commandes reste sur la sortie standard
    sortie=""
    ;;
  esac


## Fichier contenant les dernières erreurs
fichier_erreur="/tmp/erreur_backup_config.sh.tmp"
touch "${fichier_erreur}"



# Fin des variables globales
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fichiers globaux
#
# -----------------------------------------------------------
#

# Fin des fichiers globaux
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fonctions globales
#
# -----------------------------------------------------------
## Fonction de sauvegarde
# Prototype: backup $1 $2 $3 [$4]
# $1: Description de la sauvegarde
# $2: Répertoire/fichier à sauvegarder
# $3: Emplacement où stocker le répertoire/fichier
# $4: Filtre à appliquer si présent
backup()
{
  if [ ! -d $3 ]; then # Si l'emplacement de sauvegarde n'existe pas
    echod "Creation de $3"
    mkdir -p $3
  fi

  echod "Sauvegarde de $1"

  if [ $4 ]; then
    # Synchronisation avec le filtre
    #rsync -rl --delete --filter "- $4" $2 $3
    rsync -rlpgoD --delete --filter "- $4" $2 $3
  else
    # Synchronisation classique
    #rsync -rl --delete $2 $3
    rsync -rlpgoD --delete $2 $3
  fi
  # r: Récurif dans les répertoires
  # l: Copier les liens comme des liens
  # Pour appliquer l'option -a (rlptgoD), il faudrait copier les données sur le même type de partition et non pas sur du ntfs/fat
}

## Fonction de restauration
# Prototype: restore $1 $2 $3
# $1: Répertoire/fichier à restaurer
# $2: Emplacement où restaurer le répertoire/fichier
# $3: Droits sur le répertoire/fichier
#restore()
#{

#}

# Possibilité d'utiliser cp
#cp --preserve=mode,ownership,timestamps ...



## Fonction d'affichage en fonction du mode debug choisi
echod() { [ "$mode_debug" -ge 2 ] && echo "(DEBUG) $*" ; }
echok() { [ "$mode_debug" -ge 2 ] && echo "(ok) $*" ; }
echoi() { [ "$mode_debug" -ge 1 ] && echo "(ii) $*" ; }
echow() { [ "$mode_debug" -ge 1 ] && echo "(!!) $*" ; }
echnk() { [ "$mode_debug" -ge 0 ] && echo "(EE) $*" ; }

# Fin des fonctions globales
# -----------------------------------------------------------
# **********************************************************************************************



# **********************************************************************************************
#
# Programme principale
#
# -----------------------------------------------------------

# Vérification de l'existence des emplacements de sauvegarde
if [ ! -d $BKP_DIR ] || [ ! -d $BKP_DIR_USER ]; then
  echo << EOF "
L'un des emplacements de sauvegarde spécifié n'existe pas:
- $BKP_DIR
ou
- $BKP_DIR_USER

Vérifier si les variable BKP_DIR et BKP_DIR_USER contiennent les bonnes valeurs.
Si c'est le cas, merci de créer manuellement les répertoires."
EOF
  exit 1
fi


case "$1" in
  backup)
    if [ "${USER}" = "root" ]; then # Si le script est exécuté en root
      echo "Exécution de la sauvegarde en tant que root"

      ## Répertoire /etc
      # APT
      backup "APT" /etc/apt/sources.list $BKP_DIR/etc/apt/

      # Fichier fstab
      backup "fstab" /etc/fstab $BKP_DIR/etc/

      # Fichier hosts
      backup "hosts" /etc/hosts $BKP_DIR/etc/

      # Fichier interfaces (réseau)
      backup "interfaces" /etc/network/interfaces $BKP_DIR/etc/network/

      # Samba
      backup "Samba" /etc/samba $BKP_DIR/etc/

      # ZSH
      backup "ZSH" /etc/zsh $BKP_DIR/etc/
      backup "dir_colors" /etc/dir_colors $BKP_DIR/etc/


      # Répertoire /usr
      # VIM (dictionnaire)
#      backup /usr/share/vim/vim73/spell/

      # Hammerfight (jeux)
      backup "Hammerfight" /usr/local/games/hammerfight/Saves $BKP_DIR/usr/local/games/hammerfight/


    else # Si c'est un utilisateur "normal"
      echo "Exécution de la sauvegarde en tant que $USER"

      ## Applications
      # EOG (Eye Of Gnome)

      ## Devtodo
      # 644/ .todo
      backup "Devtodo" "$USER_DIR"/.todo "$BKP_DIR_USER"

      ## dmenu
      # 644
      backup "dmenu" "$USER_DIR"/.dmenu "$BKP_DIR_USER"

      # Filezilla
      backup "Filezilla" "$USER_DIR"/.filezilla "$BKP_DIR_USER"

      # Freeplane
      backup "Freeplane" "$USER_DIR"/.freeplane "$BKP_DIR_USER"

      # Gedit

      # Gnome

      # Gsimplecal
      backup "Gsimplecal" "$USER_DIR"/.config/gsimplecal "$BKP_DIR_USER"/.config/

      # Icedove
      #backup "Icedove" "$USER_DIR"/.icedove "$BKP_DIR_USER" ImapMail
      backup "Icedove" "$USER_DIR"/.icedove "$BKP_DIR_USER"

      # Iceweasel
      backup "Iceweasel" "$USER_DIR"/.mozilla "$BKP_DIR_USER"
      # Extension: Pentadactyl
      backup "Pentadactyl" "$USER_DIR"/.pentadactyl "$BKP_DIR_USER"
      backup "Pentadactyl" "$USER_DIR"/.pentadactylrc "$BKP_DIR_USER"

      # icônes
      backup "Icônes" "$USER_DIR"/.icons "$BKP_DIR_USER"

      # Irssi
      backup "Irssi" "$USER_DIR"/.irssi "$BKP_DIR_USER"

      # Jitsi
      backup "Jitsi" "$USER_DIR"/.jitsi "$BKP_DIR_USER"

      # Lessfilter
      backup "Lessfilter" "$USER_DIR"/.lessfilter "$BKP_DIR_USER"

      # Libreoffice (templates, dictionnaires, ...)
      backup "Libreoffice" "$USER_DIR"/.libreoffice "$BKP_DIR_USER"

      # MOC (Music On Console)
      backup "MOC" "$USER_DIR"/.moc "$BKP_DIR_USER"

      # Modèles de documents
      backup "Modeles" "$USER_DIR"/Modeles "$BKP_DIR_USER"
      backup "user-dirs.dirs" "$USER_DIR"/.config/user-dirs.dirs "$BKP_DIR_USER"

      # Nautilus (script)
      backup "nautilus-scripts" "$USER_DIR"/.gnome2/nautilus-* "$BKP_DIR_USER"/.gnome2/

      # Openbox
      backup "openbox" "$USER_DIR"/.config/openbox "$BKP_DIR_USER"/.config/

      # Pidgin (configuration et historique)
      backup "Pidgin" "$USER_DIR"/.purple "$BKP_DIR_USER"

      # Programme par défaut
      backup "Programme par défaut" "$USER_DIR"/.local/share/applications/mimeapps.list "$BKP_DIR_USER"/.local/share/applications

      # Pyload
      backup "Pyload" "$USER_DIR"/.pyload "$BKP_DIR_USER"

      # Rhythmbox (configuration, bibliothèque, ...)
      backup "Rhythmbox" "$USER_DIR"/.local/share/rhythmbox "$BKP_DIR_USER"/.local/share/
      backup "Rhythmbox" "$USER_DIR"/.gconf/apps/rhythmbox "$BKP_DIR_USER"/.gconf/apps/

      # Roxterm
      # 755: récursif sur les répertoires
      # 644: récursif sur les fichiers
      backup "Roxterm" "$USER_DIR"/.config/roxterm.sourceforge.net "$BKP_DIR_USER"/.config/

      # Screen
      backup "Screen" "$USER_DIR"/.screen "$BKP_DIR_USER"

      # Script
      backup "Script" "$USER_DIR"/bin "$BKP_DIR_USER"

      # SSH
      backup "SSH" "$USER_DIR"/.ssh "$BKP_DIR_USER"

      # Teamviewer
      backup "Teamviewer" "$USER_DIR"/.teamviewer "$BKP_DIR_USER"

      # Terminator
      backup "Terminator" "$USER_DIR"/.config/terminator "$BKP_DIR_USER"/.config/

      # Tmux
      backup "Tmux" "$USER_DIR"/.tmux.conf "$BKP_DIR_USER"/

      # Transmission
      backup "Transmission" "$USER_DIR"/.config/transmission "$BKP_DIR_USER"/.config

      # Tsclient
      backup "Tsclient" "$USER_DIR"/.tsclient "$BKP_DIR_USER"

      # VIM
      backup "VIM" "$USER_DIR"/.vim "$BKP_DIR_USER"
      backup "VIM" "$USER_DIR"/.vimrc "$BKP_DIR_USER"

      ## Jeux
      # Cave story +


      # Hammerfight
      # Sauvegarde effectuée par root car nécessite des privilèges

      # Minecraft
      backup "Minecraft" "$USER_DIR"/.minecraft "$BKP_DIR_USER"

      # Minetest
      backup "Minetest" "$USER_DIR"/.minetest "$BKP_DIR_USER"

      # Shank


      # Teeworlds
      backup "Teeworlds" "$USER_DIR"/.teeworlds "$BKP_DIR_USER"

      # vvvvvv
      # 755: .vvvvvv
      # 644 pour tous les fichiers
      backup "vvvvvv" "$USER_DIR"/.vvvvvv "$BKP_DIR_USER"

    fi
    ;;

  * )
      echo "Fonctionnement:"
      echo "./backup_config.sh backup|restore"

esac # Fin du case

exit 0
# Fin de la boucle principale
# -----------------------------------------------------------
# **********************************************************************************************


