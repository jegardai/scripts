#!/bin/sh

PICTURE_DIR="${1}"
# New tag to add to the picture
NEW_TAG="description"
# Temp file to store the list of directories
TMP_LIST_DIR="/tmp/list_dir_to_tag"

# List directories
find "${PICTURE_DIR}" -mindepth 1 -type d > "${TMP_LIST_DIR}"

while IFS= read -r DIR
do
	TAG_VALUE=$(basename "${DIR}")
	printf '%s\n' "Add the tag '${TAG_VALUE}' to all images in ${DIR}"

	# Add exif tag to all images of the current directory (not subdir)
	find "${DIR}" -iregex '.*\.\(jpg\|gif\|png\|jpeg\)$' -maxdepth 1 -exec exiftool -overwrite_original -P -"${NEW_TAG}"="${TAG_VALUE}" {} \;
done < "${TMP_LIST_DIR}"

exit 0
