## Script to load in Qutebrowser configuration file
## config.source('~/bin/save_qutebrowser_buffers.py

## Add "save-window-and-buffers" function in Qutebrowser
## in order to save buffers to a temp file

## Thanks to aeghn !
## https://gist.github.com/aeghn/0b5db63c3d4290fa487aa48ab7f1264c#file-save_buffers-py

from qutebrowser.utils import objreg
from qutebrowser.api import interceptor, cmdutils

@cmdutils.register()
def save_window_and_buffers():
    """
    Save buffer list
    like:
    window_id/buffer_id buffer_title
    """
    qbuffers = open('/tmp/qutebrowser_buffers_zsbd', "w")
    print_buffers = ''
    for windows_id in objreg.window_registry:
        tabbed_browser = objreg.get('tabbed-browser', scope='window',
                                    window=windows_id)
        for buffer_id in range(tabbed_browser.widget.count()):
            print_buffers = print_buffers+str(windows_id)+'/'+str(buffer_id+1)+' '+str(tabbed_browser.widget.widget(buffer_id).title())+' '+str(tabbed_browser.widget.widget(buffer_id).url())+'\n'
    qbuffers.write(str(print_buffers))
    qbuffers.close()
