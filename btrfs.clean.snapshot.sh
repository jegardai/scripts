#!/bin/bash

file="${1}"

while read a
do snapshot=$(echo ${a%%/$file})
	printf '%s' "btrfs property set ${snapshot} ro false"
	printf '%s' "rm --force -- ${a}"
	printf '%s' "btrfs property set ${snapshot} ro true"
	#btrfs property set "${snapshot}" ro false
	#rm --force -- "${a}"
	#btrfs property set "${snapshot}" ro true
done < <(find /.snapshots/ -name "${file}")

exit 0
