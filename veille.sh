#!/bin/sh

# Description:      Kill all ssh connections
#                   Ensure to kill all ssh-agent
#                   Ensure to kill all gpg-agent
#                   Ensure to delete everything link to {ssh,gpg}-agent
#                   Ensure to kill all git-credential sockets
#                   Kill keepass2
#                   Remove cached password
#                   Remove recent files
#                   Turn off the screen
#                   Lock the screen
### Give an easy way to call this function with a keybind.
## For example with herbstluftwm: "hc keybind $Mod-Shift-c spawn ~/bin/veille.sh"

## depend: i3lock

# SSH Connections
killall ssh
rm --recursive --force -- ~/.ssh/socks/*

# SSH Agent
killall ssh-agent
rm --recursive --force -- /tmp/ssh-*

# GPG Agent
keychain --agents gpg --clear
pkill --uid="$(id -u)" -- "gpg-agent"

# Delete everything link to ssh-agent or gpg-agent
rm --recursive --force -- ~/.keychain/*

# git-credential sockets
pkill --uid="$(id -u)" --full -- "git-credential-cache"
rm --force -- ~/.git-credential-cache/*

# Keepass2 (killall keepass, both those launch with 'mono' and those launch with 'cli)
pkill --uid="$(id -u)" --full -- "keepass"

# Delete sensitive files can contains unwanted clear passwords
rm --force -- ~/.config/evince/print-settings ~/.config/eog/eog-print-settings.ini

# Delete GIMP's sensitives files
find ~/.config -ipath "*GIMP*" -and -iname "print-page-setup" -delete
find ~/.config -ipath "*GIMP*" -and -iname "print-settings" -delete

# Purge local user recent files
rm --force -- ~/.local/share/recently-used.xbel
rm --force -- ~/.recently-used
rm --force --recursive -- ~/.thumbnails/normal
rm --force -- ~/.config/vlc/vlc-qt-interface.conf
rm --force -- ~/.config/smplayer/smplayer.ini

# Turn off the screen
sleep 3; xset dpms force off

# Lock the screen
#i3lock -c 000000 --pointer win -d
i3lock --image=/home/jegardai/Images/system/empire_needs_you.png --pointer win -d -e -t
