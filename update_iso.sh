#!/bin/bash

dl_iso() {
  ## URL to download
  URL="${1}"
  ## Path to store the downloaded file
  ISO_PATH="${2}"
  ## Retention time
  DATE=$(date --date '30 days ago' +%Y-%m-%d)

  if [ ! -f ${ISO_PATH} ]; then
    wget "${URL}" -O "${ISO_PATH}"
  else
    # If the file doesn't already exist and not older than 30 days
    if [ ! `find . -samefile ${ISO_PATH} -newerct ${DATE}` ]; then
      wget "${URL}" -O "${ISO_PATH}"
    fi
  fi
}

# Debian {{{
DEBIAN_STABLE="9.5.0"
## Standard Stable versions
# AMD64 (~300Mo)
dl_iso "http://cdimage.debian.org/debian-cd/${DEBIAN_STABLE}/amd64/iso-cd/debian-${DEBIAN_STABLE}-amd64-netinst.iso" "LINUX/debian-stable-amd64-netinst.iso"
# i386 (~400Mo)
dl_iso "http://cdimage.debian.org/debian-cd/${DEBIAN_STABLE}/i386/iso-cd/debian-${DEBIAN_STABLE}-i386-netinst.iso" "LINUX/debian-stable-i386-netinst.iso"
rm -f LINUX/debian-stable ; echo "${DEBIAN_STABLE}" > "LINUX/debian-stable"

## Version with firmares
# AMD64 (~350Mo)
dl_iso "http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/${DEBIAN_STABLE}+nonfree/amd64/iso-cd/firmware-${DEBIAN_STABLE}-amd64-netinst.iso" "LINUX/firmware-stable-amd64-netinst.iso"
# i386 (~412Mo)
#dl_iso "http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/${DEBIAN_STABLE}+nonfree/i386/iso-cd/firmware-${DEBIAN_STABLE}-i386-netinst.iso" "LINUX/firmware-stable-i386-netinst.iso"

## Live with firmares
# AMD64 + Cinnamon (~2,5Go)
dl_iso "http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/${DEBIAN_STABLE}-live+nonfree/amd64/iso-hybrid/debian-live-${DEBIAN_STABLE}-amd64-cinnamon+nonfree.iso" "LINUX/debian-live-amd64-cinnamon-nonfree.iso"

## Testing
# AMD64 (~350Mo)
dl_iso "http://cdimage.debian.org/cdimage/daily-builds/daily/arch-latest/amd64/iso-cd/debian-testing-amd64-netinst.iso" "LINUX/debian-testing-amd64-netinst.iso"
# i386 (~425Mo)
dl_iso "http://cdimage.debian.org/cdimage/daily-builds/daily/arch-latest/i386/iso-cd/debian-testing-i386-netinst.iso" "LINUX/debian-testing-i386-netinst.iso"

## Unstable
# AMD64 (~50Mo)
dl_iso "http://ftp.nl.debian.org/debian/dists/sid/main/installer-amd64/current/images/netboot/mini.iso" "LINUX/debian-unstable-mini.iso"

# }}}

# ArchLinux (~550Mo) {{{
ARCH_VERSION="2018.10.01"
dl_iso "http://archlinux.mirrors.ovh.net/archlinux/iso/${ARCH_VERSION}/archlinux-${ARCH_VERSION}-dual.iso" "LINUX/archlinux-dual.iso"
rm -f LINUX/archlinux-version ; echo "${ARCH_VERSION}" >  "LINUX/archlinux-version"

# }}}

# Linux Mint {{{
LM_LAST="19"
LMDE_LAST="201808"
## Linux Mint
#dl_iso "http://ftp.crifo.org/mint-cd/stable/${LM_LAST}/linuxmint-${LM_LAST}-cinnamon-64bit-v2.iso"  "LINUX/linuxmint-"${LM_LAST}"-cinnamon-64bit-v2.iso"
## LMDE Cinnamon
# (~1,7Go)
dl_iso "http://ftp.crifo.org/mint-cd/debian/lmde-3-${LMDE_LAST}-cinnamon-64bit.iso" "LINUX/lmde-3-"${LMDE_LAST}"-cinnamon-64bit.iso"

# }}}

# Tails (~1,2Go) {{{
TAILS_VERSION="3.9.1"
dl_iso "http://dl.amnesia.boum.org/tails/stable/tails-amd64-${TAILS_VERSION}/tails-amd64-${TAILS_VERSION}.iso" "LINUX/tails-amd64-${TAILS_VERSION}.iso"

# }}}

# Kali (~2,9Go) {{{
KALI_VERSION="2018.3a"
KALI_WEEKLY="2018-W41"
#dl_iso "http://cdimage.kali.org/kali-weekly/kali-linux-e17-"${KALI_WEEKLY}"-amd64.iso" "LINUX/kali-linux-e17-"${KALI_WEEKLY}"-amd64.iso"
#dl_iso "http://cdimage.kali.org/kali-${KALI_VERSION}/kali-linux-e17-${KALI_VERSION}-amd64.iso" "LINUX/kali-linux-e17-"${KALI_VERSION}"-amd64.iso"

# }}}

# Clonezilla (~150Mo) {{{
CLONEZILLA_STABLE="2.5.6-22"
dl_iso "http://downloads.sourceforge.net/project/clonezilla/clonezilla_live_stable/${CLONEZILLA_STABLE}/clonezilla-live-${CLONEZILLA_STABLE}-amd64.iso" "BACKUP_LINUX/clonezilla-live-${CLONEZILLA_STABLE}-amd64.iso"
dl_iso "http://downloads.sourceforge.net/project/clonezilla/clonezilla_live_stable/${CLONEZILLA_STABLE}/clonezilla-live-${CLONEZILLA_STABLE}-i686-pae.iso" "BACKUP_LINUX/clonezilla-live-${CLONEZILLA_STABLE}-i686-pae.iso"
rm -f BACKUP_LINUX/clonezilla-live-stable ; echo "${CLONEZILLA_STABLE}" > "BACKUP_LINUX/clonezilla-live-stable"

# }}}

# Kaspersky (~200Mo) {{{
dl_iso "http://rescuedisk.kaspersky-labs.com/rescuedisk/updatable/2018/krd.iso" "ANTIVIRUS/kaspersky_anti_virus_rescue_cd_10.iso"

# }}}

# Gparted (~200Mo) {{{
GPARTED_VERSION="0.32.0-1"
dl_iso "http://downloads.sourceforge.net/project/gparted/gparted-live-stable/${GPARTED_VERSION}/gparted-live-${GPARTED_VERSION}-amd64.iso" "UTILITIES/gparted-live-${GPARTED_VERSION}-amd64.iso"

# }}}

# SystemrescueCD (~400Mo) {{{
SYSRCCD_VERSION="5.3.1"
dl_iso "http://downloads.sourceforge.net/project/systemrescuecd/sysresccd-x86/${SYSRCCD_VERSION}/systemrescuecd-x86-${SYSRCCD_VERSION}.iso" "UTILITIES/systemrescuecd-x86-${SYSRCCD_VERSION}.iso"

# }}}

# Memtest {{{
MEM_VERSION="5.01"
dl_iso "http://www.memtest.org/download/${MEM_VERSION}/memtest86+-${MEM_VERSION}.iso.zip" "UTILITIES/memtest86+-${MEM_VERSION}.iso.zip"
unzip UTILITIES/memtest86+-${MEM_VERSION}.iso.zip -d UTILITIES/
rm -f UTILITIES/memtest86+-${MEM_VERSION}.iso.zip

# }}}

# Bootdisk // Reset windows password {{{
## http://pogostick.net/~pnh/ntpasswd/
NT_VERSION="140201"
dl_iso "http://pogostick.net/~pnh/ntpasswd/cd${NT_VERSION}.zip" "UTILITIES/cd${NT_VERSION}.zip"
unzip UTILITIES/cd${NT_VERSION}.zip -d UTILITIES/
rm -f  UTILITIES/cd${NT_VERSION}.zip

# }}}
