#!/bin/sh

git filter-branch --env-filter '
OLD_EMAIL="old_username@email"
CORRECT_NAME="LastName FirstName"
CORRECT_EMAIL="username@email"
if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags

printf '%b' "You can check you git history, then run :\n"
printf '%b' "git push --force --tags origin 'refs/heads/*'"

exit 0
