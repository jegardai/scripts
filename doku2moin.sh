#!/bin/sh
################################################################################################
##
##  Nom: doku2moin.sh
##
##  Version: 0.1
##
##  Licence: Creative Commons
##
##  Comportement: Transformer une page de Dokuwiki est en page compatible avec le wiki Moinmoin.
##
##
##
################################################################################################
##
##  Date de création: 15-12-2011
##
##  Auteur: Gardouille
##
##
##  Dernière modifications:
##  15-12-2011 - 0.1: Création du script
##    -
# **********************************************************************************************
##  //2011 - :
##    -
##    -
# **********************************************************************************************
##  À Modifier:
##    - Ajouter l'entête pour la syntaxe Moinmoin automatiquement dans le fichier
##    - Ajouter une transformation pour:
##      - http://packages.debian.org/search?keywords=NOM_PAQUET -> [[DebianPkg:NOM_PAQUET|Détail du paquet]]
##      - http://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=NOM_PAQUET -> [[DebianBug:NOM_PAQUET|Rapport de bug NOM_PAQUET]]
##    - Modifier la transformation de // en '' pour ne pas prendre http:// (par exemple)
##
##
################################################################################################



# **********************************************************************************************
#
# Variables globales
#
# -----------------------------------------------------------
# 

## Choix du mode d'exécution du script:
## Niveau 0 (mode_debug=0)
# Exécution automatique avec normalement aucune interaction avec l'utilisateur.
## Niveau 1 (mode_debug=1)
# ...
## Niveau 2 (mode_debug=2)
# L'installation n'est pas en automatique (aptitude, ...)
# La sortie des différentes commandes est envoyée sur la sortie standard
mode_debug=2

case "$mode_debug" in
  0) ## Niveau minimum
    # Les questions d'aptitude seront automatiquement validées
    options="-y"
    # La sortie standard sera redirigée dans un fichier de log
    sortie="> /dev/null 2> \"${fichier_erreur}\""
    ;;
  1) ## Niveau moyen
    # Les questions d'aptitude seront automatiquement validées
    options="-y"
    # La sortie standard sera quand même affichée
    sortie="2> \"${fichier_erreur}\""
    ;;
  2) ## Niveau maximum
    # Aucune options pour aptitude
    options=""
    # Le résultat des commandes reste sur la sortie standard
    sortie=""
    ;;
  esac


## Fichier contenant les dernières erreurs
fichier_erreur="/tmp/erreur_doku2moin.sh.tmp"
touch "${fichier_erreur}"

## Fichier à modifier
file_in=$1
## Fichier modifié
file_out="$1.moin"
## Fichier contenant toutes les expressions à appliquer
expression="sed_expression"

## Liste des expressions sed à appliquer à la commande
titre1="s/^====== /= /\ns/ ======$/ =/\n"
titre2="s/^===== /== /\ns/ =====$/ ==/\n"
titre3="s/^==== /=== /\ns/ ====$/ ===/\n"
titre4="s/^=== /==== /\ns/ ===$/ ====/\n"
titre5="s/^== /===== /\ns/ ==$/ =====/\n"
titre6="s/^= /====== /\ns/ =$/ ======/\n"
code="s/<code>/{{{/\ns/<\/code>/}}}/\n"
## cmd bash:
## dokuwiki: <code bash>
## moinmoin: {{{#!highlight bash
cmd_bash="s/<code \(bash\)>/{{{#!highlight \\\1/\n"
file="s/<file>/{{{/\ns/<\/file>/}}}/\n"
file_type="s/<file \(.*\) .*/{{{#!highlight \\\1/\n"
gras="s/\*\*/\'\'\'/g\n"
italique="s/\/\//\'\'/g\n"
#lien_externe similaire
#liste_puce similaire (-e 's/\(^ \)\*/\1\*/')
liste_num="s/\(^ *\)-/\\\11\./\n"
## Image:
# dokuwiki: {{:teeworlds_01.png|}}
# moinmoin: {{attachment:teeworlds_01.png}}
image="s/{{:\(.*\)|}}/{{attachement:\\\1}}/\n"



# Fin des variables globales
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fichiers globaux
#
# -----------------------------------------------------------
#

# Fin des fichiers globaux
# -----------------------------------------------------------
# **********************************************************************************************


# **********************************************************************************************
#
# Fonctions globales
#
# -----------------------------------------------------------
## Fonction d'affichage en fonction du mode debug choisi
echod() { [ "$mode_debug" -ge 2 ] && echo "(DEBUG) $*" ; }
echok() { [ "$mode_debug" -ge 2 ] && echo "(ok) $*" ; }
echoi() { [ "$mode_debug" -ge 1 ] && echo "(ii) $*" ; }
echow() { [ "$mode_debug" -ge 1 ] && echo "(!!) $*" ; }
echnk() { [ "$mode_debug" -ge 0 ] && echo "(EE) $*" ; }

# Fin des fonctions globales
# -----------------------------------------------------------
# **********************************************************************************************



# **********************************************************************************************
#
# Programme principale
#
# -----------------------------------------------------------

echo $titre1$titre2$titre3$titre4$titre5$titre6$code$cmd_bash$file$file_type$gras$italique$liste_num$image > $expression

sed --file="${expression}" "${file_in}" > "${file_out}"

echo "-------------------------------"
echo "-------------------------------"
echo "Rajouter au début du fichier (avant le titre):"
echo "#language fr
||<tablestyle=\"width: 100%; border: 0px hidden\">~-Traduction(s): [[$file_in|English]] - Français||
  ----

<<TableOfContents()>>"

exit 0
# Fin de la boucle principale
# -----------------------------------------------------------
# **********************************************************************************************


