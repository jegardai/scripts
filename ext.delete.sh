#!/bin/sh

# Purpose : Remove file's extension (lasts characters after the last dot)

# Vars {{{
readonly PROGNAME=$(basename "${0}")
readonly NBARGS="${#}"
## Test if DEBUG is already defined (by parent script,…)
[ -z "${DEBUG}" ] && DEBUG=1

## Colors
readonly PURPLE='\033[1;35m'
readonly RED='\033[0;31m'
readonly RESET='\033[0m'
readonly COLOR_DEBUG="${PURPLE}"
# }}}
usage() {                                                       # {{{

	cat <<- EOF
usage: $PROGNAME [--debug,--help]

Try to remove file's extension.

EXAMPLES :
    - Delete "bkp" extension to /tmp/test.me.bkp file
        ${PROGNAME} /tmp/test.me.bkp

OPTIONS :
    -d,--debug
        Enable debug messages.

    -h,--help
        Print this help message.

EOF

}
# }}}
debug_message() {                                               # {{{

	local_debug_message="${1}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '\e[1;35m%-6b\e[m\n' "DEBUG − ${PROGNAME} : ${local_debug_message}"

	local_debug_message=""

	return 0
}
# }}}
error_message() {                                               # {{{

	local_error_message="${1}"
	local_error_code="${2}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '%b\n' "ERROR − ${PROGNAME} : ${RED}${local_error_message}${RESET}"

	local_error_message=""

	exit "${local_error_code:=66}"
}
# }}}
is_file() {                                                     # {{{

	local_file="${1}"

	if [ -e "${local_file}" ]; then
		return_is_file="0"
		debug_message "is_file − \
${local_file} seems to exists."
	else
		return_is_file="1"
		debug_message "is_file − \
${local_file} doesn't seems to exists."
	fi

	## Empty var
	local_file=""

	return "${return_is_file}"

}
# }}}
is_not_file() {                                                 # {{{

	local_file="${1}"

	if [ ! -e "${local_file}" ]; then
		return_is_not_file="0"
		debug_message "is_not_file − \
${local_file} doesn't seems to exists."
	else
		return_is_not_file="1"
		debug_message "is_not_file − \
${local_file} seems to exists."
	fi

	## Empty var
	local_file=""

	return "${return_is_not_file}"

}
# }}}
is_defined() {                                                  # {{{

	local_var="${1}"
	local_var_name="${2}"

	if [ -n "${local_var}" ]; then
		return_is_defined="0"
		debug_message "is_defined − \
${local_var_name:=VAR} (${local_var}) seems defined."
	else
		return_is_defined="1"
		debug_message "is_defined − \
${local_var_name:=VAR} doesn't seems defined."
	fi

	## Empty var
	local_var=""
	local_var_name=""

	return "${return_is_defined}"

}
# }}}
is_not_defined() {                                              # {{{

	local_var="${1}"
	local_var_name="${2}"

	if [ -z "${local_var}" ]; then
		return_is_not_defined="0"
		debug_message "is_not_defined − \
${local_var_name:=VAR} doesn't seems defined."
	else
		return_is_not_defined="1"
		debug_message "is_not_defined − \
${local_var_name:=VAR} (${local_var}) seems defined."
	fi

	## Empty var
	local_var=""
	local_var_name=""

	return "${return_is_not_defined}"

}
# }}}
remove_extension() {                                            # {{{

	local_file="${1}"

	file_without_extension=$(echo "${local_file}" | sed -e 's/\.[^.]*$//')

	debug_message "remove_extension − \
Try to remove extension to ${local_file} file"
	mv -- "${local_file}" "${file_without_extension}"

	return_remove_extension="${?}"

	## Empty var
	local_file=""
	file_without_extension=""

	return "${return_remove_extension}"

}
# }}}

main() {                                                        # {{{

	is_not_defined "${FILE_LIST}" FILE_LIST && \
		usage && \
		error_message "Some file or directory must be passed as argument" "33"

	IFS="%"
	for file in ${FILE_LIST}; do

		debug_message "FILE_LIST for loop − \
Try to ${file} file"

		remove_extension "${file}"

	done

}
# }}}

# Manage arguments                                                # {{{

## If there is argument(s)
if [ ! "${NBARGS}" -eq "0" ]; then

	manage_arg="0"

	# Parse all options (start with a "-") one by one {{{
	while printf -- '%s' "${1}" | grep -q -E -- "^-+"; do

	case "${1}" in
		-d|--debug )           ## Enable debug mode
			## Enable DEBUG
			DEBUG="0"
			;;
		-h|--help )            ## help
			usage
			## Exit after help informations
			exit 0
			;;
		* )                    ## unknow option
			printf '%b\n' "${RED}Invalid option: ${1}${RESET}"
			printf '%b\n' "---"
			usage
			exit 1
			;;
	esac

	debug_message "Arguments management − \
${RED}${1}${COLOR_DEBUG} option managed."

	## Move to the next argument
	shift
	manage_arg=$((manage_arg+1))

	done
	# }}}

	# If it remains arguments (not option) {{{
	if [ "${1}" ]; then
		debug_message "Arguments management − \
Remains arguments."
		while printf -- '%s' "${1}" | grep -q -E -- "^.+"; do
			temp_arg="${1}"

			debug_message "Arguments management − \
Manage ${temp_arg}"
			### If arg is a file|directory|…
			is_file "${temp_arg}" && {
				### Add arg to FILE_LIST if it's already defined
				is_defined "${FILE_LIST}" FILE_LIST && \
					FILE_LIST="${FILE_LIST}%${temp_arg}" || \
					FILE_LIST="${temp_arg}"
			}

			### If arg is not a file|directory|…
			is_not_file "${temp_arg}" && \
				printf '%s\n' "${temp_arg} doesn't seems to exist, skipping."

			### Move to the next argument
			shift
			manage_arg=$((manage_arg+1))
		done
	else
		debug_message "Arguments management − \
No more arguments."
	fi
	# }}}

	debug_message "Arguments management − \
${RED}${manage_arg}${COLOR_DEBUG} argument(s) successfully managed."
else
	debug_message "Arguments management − \
No arguments/options to manage."
fi

# }}}

main

exit 0
