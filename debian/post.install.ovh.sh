#!/bin/sh

# Purpose {{{
# This script should be called at the end of a OVH Debian installation
#   1. Update APT repositories and install few dependencies.
#   2. Umount and remove temp Logical Volume (with all free space).
#   3. Allow SSH connection with a password for root.
#   4. Download and extract a latecommand archive.
#   5. Source /etc/os-release file to have $VERSION_CODENAME variable.
#   6. Run last post-install script dedicated to Debian Codename.
#
# 2023-06-26
# }}}
# Flags {{{
## Exit on error {{{
set -o errexit
## }}}
## Exit on unset var {{{
### Use "${VARNAME-}" to test a var that may not have been set
set -o nounset
## }}}
## Pipeline command is treated as failed {{{
### Not available in POSIX sh − https://github.com/koalaman/shellcheck/wiki/SC3040
#set -o pipefail
## }}}
## Help with debugging {{{
### Call the script by prefixing it with "TRACE=1 ./script.sh"
if [ "${TRACE-0}" -eq 1 ]; then set -o xtrace; fi
## }}}
# }}}

# Update repositories
apt update
# Install few dependencies
apt --assume-yes install -- wget

# Remove last partition with all free space
if [ -d /mnt/free ]; then
	umount /mnt/free
	test -f /dev/mapper/vg-free && lvremove --force /dev/mapper/vg-free
	/bin/rmdir -- /mnt/free
	/bin/sed --in-place -- '/\/mnt\/free/d' /etc/fstab
fi

# Allow root connection with password
/bin/sed --in-place -- 's/\(^\|^\#\)\(PermitRootLogin\).*/\2 yes/g' /etc/ssh/sshd_config

# Download and uncompress latecommand archive
wget --quiet --output-document=/tmp/latecommand.tar.gz -- "https://git.ipr.univ-rennes.fr/cellinfo/tftpboot/raw/branch/master/scripts/latecommand.tar.gz"
tar xzf /tmp/latecommand.tar.gz -C /tmp/
# Run dedicated script
## Source /etc/os-release to get Debian codename
. /etc/os-release
/bin/sh /tmp/latecommand/post."${VERSION_CODENAME:=bookworm}".sh

exit 0
