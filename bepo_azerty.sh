#!/bin/sh

## Petit script qui switch la disposition du clavier de bépo à azerty
##  et qui recharge la configuration de herbsluftwm pour obtenir quelque
##  chose d'utilisable dans les deux cas.

setxkbmap -print | grep bepo
## Already bepo? -> switch to azerty
if [ $? -eq 0 ]; then
  setxkbmap fr oss
  # Select the correct config file for herbstluftw
  cp ~/.config/herbstluftwm/autostart.azerty ~/.config/herbstluftwm/autostart
else
  lsusb|grep -i typematrix
  ## TypeMatrix's keyboard connected? -> switch to it
  if [ $? -eq 0 ]; then
    setxkbmap fr bepo
  else
    setxkbmap fr bepo
  fi
  # Select the correct config file for herbstluftw
  cp ~/.config/herbstluftwm/autostart.bepo ~/.config/herbstluftwm/autostart
fi

## Reload the herbstluftwm config file
herbstclient reload
