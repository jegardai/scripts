#! /bin/sh

# Description:      Manage video output (size, orientation, ...)
#                   And reload wm configuration

# Vars {{{
[ -z "${DEBUG}" ] && readonly DEBUG=0

## Colors
readonly PURPLE='\033[1;35m'
readonly RED='\033[0;31m'
readonly RESET='\033[0m'
readonly COLOR_DEBUG="${PURPLE}"

## First and primary output (laptop screen, hdmi-1,…)
## It can be forced here or the script will try to discover it's name
#readonly PRIMARY_OUTPUT_NAME="LVDS-1"

## Second output (hdmi-2, dp-1,…)
## It can be forced here or the script will try to discover it's name
#readonly SECOND_OUTPUT_NAME

# }}}

debug_message() {                                               # {{{

	local_message="${1}"

	## Print message if DEBUG is enable (=0)
	[ "${DEBUG}" -eq "0" ] && printf '\e[1;35m%-6b\e[m\n' "DEBUG − ${PROGNAME}: ${local_message}"

	return 0
}
# }}}
define_vars() {                                                 # {{{

	## Primary output {{{
	### If the primary output wasn't manually defined,
	### try to get it's name
	if [ -z "${PRIMARY_OUTPUT_NAME}" ]; then
		get_primary_output_name
	fi
	debug_message "define_vars − \
The primary output is: ${PRIMARY_OUTPUT_NAME}."
	## }}}
	## Second output {{{
	### If the second output wasn't manually defined,
	### try to get it's name
	if [ -z "${SECOND_OUTPUT_NAME}" ]; then
		SECOND_OUTPUT_NAME=$(xrandr | grep " connected" | grep -v "${PRIMARY_OUTPUT_NAME}" | awk 'NR==1{ print $1 }')
		debug_message "define_vars − \
The second output is: ${SECOND_OUTPUT_NAME}."
	fi
	## }}}

	[ -f "${HOME}/.config/xrandr.conf" ] && \
		. "${HOME}/.config/xrandr.conf" && \
		printf "%b\n" 'Use local config file.'

}
# }}}
get_primary_output_name() {                                     # {{{

	## First, check if an output is already defined as primary
	local_primary_output_name=$(xrandr | grep " primary" | grep -v " disconnected" | awk 'NR==1{ print $1 }')

	## If the var contains something
	if [ -n "${local_primary_output_name}" ]; then
		debug_message "get_primary_output_name − \
An output is defined as primary: ${local_primary_output_name}".

	else
		## Then check if an output starting with "LVD" (typically a laptop) is connected
		local_primary_output_name=$(xrandr | grep "^LVD" | grep " connected" | awk 'NR==1{ print $1 }')
		if [ -n "${local_primary_output_name}" ]; then
			debug_message "get_primary_output_name − \
The primary output seems to be a laptop screen: ${local_primary_output_name}".

		else
			## Then check if any output is connected and take the first one
			local_primary_output_name=$(xrandr | grep " connected" | awk 'NR==1{ print $1 }')
			if [ -n "${local_primary_output_name}" ]; then
				debug_message "get_primary_output_name − \
Take the first connected output as primary: ${local_primary_output_name}".

			else
				debug_message "get_primary_output_name − \
Unable to get primary output name!
Ensure to have a primary, LVDS or any connected output in xrandr."
				exit 2
			fi
		fi
	fi

	readonly PRIMARY_OUTPUT_NAME="${local_primary_output_name}"
}
# }}}
is_proc_running() {                                             # {{{

	local_proc_pattern="${1}"

	local_count_proc_pattern="$(pgrep -f -- "${local_proc_pattern}" | wc -l)"

	case "${local_count_proc_pattern}" in
		0 ) ## No procs related to this pattern are running
			return_proc_running="1"
			;;
		* ) ## At least one proc seems running
			return_proc_running="0"
			;;
esac

	## Simple debug message to valid current variables
	debug_message "is_proc_running − \
procs running (with the pattern: ${RED}${local_proc_pattern}${COLOR_DEBUG}) on the current host: ${RED}${local_count_proc_pattern}${COLOR_DEBUG}."

	return "${return_proc_running}"

}
# }}}
main() {                                                        # {{{

	## Update herbstluftwm Window Manager configuration if it's running
	is_proc_running "herbstluftwm" \
		&& herbstclient detect_monitors \
		&& herbstclient reload
}
# }}}

# Define all vars
define_vars

# Manage arguments                                                # {{{
# This code can't be in a function due to arguments
case "${1}" in
	primary | off )
		## TODO : One command to enable primary screen and a loop to disable all other connected output
		if [ -n "${PRIMARY_OUTPUT_MODE}" ]; then
			xrandr --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --mode "${PRIMARY_OUTPUT_MODE}"
		else
			xrandr --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --auto
		fi
		if [ -n "${SECOND_OUTPUT_NAME}" ]; then xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --off ; fi
		printf "%b\n" 'Enable only primary output'
		;;
	leftof | left-of | ds | dock | dockstation | work )
		#xrandr --output VGA-1 --mode 1024x768 --left-of "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --mode 1366x768
		if [ -n "${PRIMARY_OUTPUT_MODE}" ]; then
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --left-of "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --mode "${PRIMARY_OUTPUT_MODE}"
		else
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --left-of "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --auto
		fi
		printf "%b\n" 'Xrandr for second output left-of primary screen'
		;;
	rightof | right-of | home )
		if [ -n "${PRIMARY_OUTPUT_MODE}" ]; then
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --right-of "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --mode "${PRIMARY_OUTPUT_MODE}"
		else
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --right-of "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --auto
		fi
		printf "%b\n" 'Xrandr for second output left-of primary screen'
		;;
	above )
		if [ -n "${PRIMARY_OUTPUT_MODE}" ]; then
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --above "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --mode "${PRIMARY_OUTPUT_MODE}"
		else
			xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --above "${PRIMARY_OUTPUT_NAME:=/dev/null}" --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --auto
		fi
		printf "%b\n" 'Xrandr for second output above primary screen'
		;;
	out | game | second )
		xrandr --output "${SECOND_OUTPUT_NAME:=/dev/null}" --auto --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --off
		printf "%b\n" 'Xrandr for second output only'
		;;
	primary_off | first_off | main_off | laptop_off )
		xrandr --output "${PRIMARY_OUTPUT_NAME:=/dev/null}" --off
		printf "%b\n" 'Xrandr to disable primary output'
		;;
	* )
		printf "%b\n" 'Bad argument, please use [primary|left-of|right-of|above|second|primary_off|...]'
		exit 1
		;;
esac

main

# }}}

exit 0
